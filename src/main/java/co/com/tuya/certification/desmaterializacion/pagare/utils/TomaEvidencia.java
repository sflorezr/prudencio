package co.com.tuya.certification.desmaterializacion.pagare.utils;

import net.serenitybdd.core.Serenity;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface TomaEvidencia {


	static void deBaseDeDatos(ResultSet evidencia) throws SQLException {
		int MetaDataRS = 0;
		String cadena="";
		MetaDataRS=evidencia.getMetaData().getColumnCount();

		while (evidencia.next()){

			for (int i = 1; i <=evidencia.getMetaData().getColumnCount(); i++) {
					cadena=cadena+" "+String.format("%-"+evidencia.getString(i).length()+"s", evidencia.getString(i));
			}
			cadena=cadena+"\n";
		}
		Serenity.recordReportData().withTitle("Evidencia de base de datos ").andContents(cadena);


	}
}
