package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class GenerarPagareService {
	public static final String END_POINT = "https://brokerdllo.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0";
	public static final String NUMERO_SOLICITUD_XML_TAG = "valorNumeroSolicitud";
	public static final String CLAVE_XML_TAG = "valorClave";
	public static final String TIPO_DOCUMENTO_XML_TAG = "valorTipoDocumento";
	public static final String NUMERO_DOCUMENTO = "valorNumeroDocumento";
	public static final String CONTENIDO = "valorContenido";
	public static final String JUST_END_POINT = "";

	private GenerarPagareService() {
	}
}
