package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookgenerarfirmasolicitud;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.XML_GENERAR_FIRMA_SOLICITUD;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.CrearUnaConexionDeBaseDeDatos;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.QueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

public class GenerarFirmaSolicitud {
	private static final String USER_SERVICE = "USER SERVICES";

	@Before
	public void setup() {
    setTheStage(new OnlineCast());
    theActorCalled(USER_SERVICE).whoCan(CallAnApi.at("https://brokerlab.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0"));
    theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_GENERAR_FIRMA_SOLICITUD));
    theActorInTheSpotlight().whoCan(QueryTheDatabase.as400());
    theActorInTheSpotlight().whoCan(CrearUnaConexionDeBaseDeDatos.conConfiguracion("qvi2slo", "QVI2SLO+jn", "jdbc:as400://10.169.104.172:23"));
	}
	
}
