package co.com.tuya.certification.desmaterializacion.pagare.abilities;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.ActorCannotReadXml;
import io.restassured.path.xml.XmlPath;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ReadXml implements Ability {
    private String pathXmlFile;
    private static final Logger LOGGER = Logger.getLogger(ReadXml.class.getName());

    public ReadXml(String pathXmlFile) {
        this.pathXmlFile = pathXmlFile;
    }

    public static ReadXml fromPath(String pathXmlFile) {
        return new ReadXml(pathXmlFile);
    }

    public static ReadXml as(Actor actor) {
        if (actor.abilityTo(ReadXml.class) == null) {
            throw new ActorCannotReadXml(actor.getName());
        }
        return actor.abilityTo(ReadXml.class);
    }

    public String resolve() {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(pathXmlFile);

        } catch (FileNotFoundException e) {
            LOGGER.error("could not read xml file from source " + pathXmlFile);
        }
        return XmlPath.from(inputStream).prettify();
    }


    @Override
    public String toString() {
        return "read a xml file at:" + pathXmlFile;
    }
}
