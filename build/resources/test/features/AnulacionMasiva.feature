# language: es
Característica: Verificacion de servicio Anulacion Masiva

  Esquema del escenario: Anulacion Masiva
    Cuando Ejecuto servicio AnulacionMasica con Esquema: "<esquema>" idPagare: "<idPagare>" idSolicitud: "<idSolciitud>"
    Entonces Valido respuesta del servicio AnulacionMasiva "<respuesta>"

    Ejemplos:
      | esquema       | idPagare | idSolciitud | respuesta |
      | vinculaciones | 542738   | 700521348   | 200       |

