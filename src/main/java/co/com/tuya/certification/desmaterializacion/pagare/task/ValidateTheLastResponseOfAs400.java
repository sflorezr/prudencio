package co.com.tuya.certification.desmaterializacion.pagare.task;

import static java.util.Arrays.asList;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.isIn;

import java.sql.SQLException;
import java.util.List;

import com.google.common.collect.ImmutableList;

import co.com.tuya.certification.desmaterializacion.pagare.questions.Consult;
import co.com.tuya.certification.desmaterializacion.pagare.questions.LastResponseAs400;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
public class ValidateTheLastResponseOfAs400 implements Task{
	private List<String> fields;
	private List<String> items;
	
	public ValidateTheLastResponseOfAs400 hasTheItems(String... items) {
		this.items = asList(items);
		return this;
	}
	public ValidateTheLastResponseOfAs400(List<String> fields) {
		this.fields=ImmutableList.copyOf(fields);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {		
		fields.forEach(field->actor.should(seeThat(LastResponseAs400.Consulta(field),isIn(items))));	
	}
	public static ValidateTheLastResponseOfAs400 ofFields(String... fields) {
		try {
			Consult.getLastResponse().next();
		} catch (SQLException e) {
			
		}
		return new ValidateTheLastResponseOfAs400(asList(fields));
	}
	

}
