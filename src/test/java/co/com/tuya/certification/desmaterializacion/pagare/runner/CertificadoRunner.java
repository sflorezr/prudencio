package co.com.tuya.certification.desmaterializacion.pagare.runner;

import static cucumber.api.SnippetType.CAMELCASE;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
        features = "src//test//resources//features//Certificados.feature"
        //,tags= {"@Ejecutar"}
        ,glue = {"co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions"}
        , snippets = CAMELCASE        

)
@RunWith(CucumberWithSerenity.class)
public class CertificadoRunner {

}
