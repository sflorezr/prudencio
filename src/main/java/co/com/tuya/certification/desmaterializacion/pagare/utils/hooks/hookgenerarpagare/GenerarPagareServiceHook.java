package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookgenerarpagare;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.XML_GENERAR_PAGARE;
import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.QueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

public class GenerarPagareServiceHook {

	private static final String USER_HOMINI = "USER HOMINI";

	@Before
	public void setup() {
        setTheStage(new OnlineCast());
        theActorCalled(USER_HOMINI).whoCan(CallAnApi.at("https://brokerdllo.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_GENERAR_PAGARE));
        theActorInTheSpotlight().whoCan(QueryTheDatabase.as400());
//        theActorCalled("checho").whoCan(CallAnApi.at("https://10.169.104.206:7083/intf/fw/ChannelAdapter/V2.0"));
//        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_VALIDACION_HUELLA));

	}
}
