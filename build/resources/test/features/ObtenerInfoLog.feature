# language: es
Característica: ObtenerInfoLog

  Esquema del escenario: Consultar informacion de solicitud desmaterializada
    Cuando Yo ejecuto servicio ObtenerInformacionLog con lo siguientes datos numeroDocumento: "<numeroDocumento>" , numeroSolicitud: "<numeroSolicitud>" y tipoDocumento: "<tipoDocumento>"
    Entonces Valido respuesta del servicio ObtenerInformacionLog con codigoRespuesta: "<codigoRespuesta>", mensajeRespuesta: "<mensajeRespuesta>", codigoDocumentoDesmaterializado: "<codigoDocumentoDesmaterializado>", nombreDocumentoDesmaterializado: "<nombreDocumentoDesmaterializado>", numeroDocumentoDesmaterializado: "<numeroSolicitud>" , idDocumento:"<idDocumento>", codigoRespuestaFirma "<codigoRespuestaFirma>", descripcionRespuestaFirma: "<descripcionRespuestaFirma>"

    Ejemplos:
      | numeroDocumento | tipoDocumento | numeroSolicitud | codigoRespuesta | mensajeRespuesta    | idDocumento | codigoDocumentoDesmaterializado | nombreDocumentoDesmaterializado | codigoRespuestaFirma | descripcionRespuestaFirma        |
      | 43256717        | 1             | 398923548       | OK00            | TRANSACCION EXITOSA | 0           | 1                               | SOLICITUD                       | OK00                 | Documento Generado Correctamente |