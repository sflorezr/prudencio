package co.com.tuya.certification.desmaterializacion.pagare.task;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.GenerarPagareResponse;
import co.com.tuya.certification.desmaterializacion.pagare.questions.TheLastResponseOfFirmaPagare;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

public class ValidateTheLastResponseOfFirmaPagare implements Task {
    private GenerarPagareResponse generarPagareResponse;

    public ValidateTheLastResponseOfFirmaPagare(GenerarPagareResponse generarPagareResponse) {
        this.generarPagareResponse = generarPagareResponse;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.should(
                seeThat(TheLastResponseOfFirmaPagare.theField("codigoRespuesta"), is(equalTo(generarPagareResponse.getCodigoRespuesta()))),
                seeThat(TheLastResponseOfFirmaPagare.theField("mensajeRespuesta"), is(equalTo(generarPagareResponse.getMensajeRespuesta())))
                /*seeThat(TheLastResponseOfFirmaPagare.responseTime(TimeUnit.MILLISECONDS), is(lessThan(9000L)))*/
        );
    }
}
