package co.com.tuya.certification.desmaterializacion.pagare.model;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ConsultarBeneficiariosRequest {
    private String numeroId="";
    private String numeroSolicitud;

    public ConsultarBeneficiariosRequest(String numeroId) {
        this.numeroId = numeroId;
    }
    public static  ConsultarBeneficiariosRequest conNumeroId(String numeroId){
        return new ConsultarBeneficiariosRequest(numeroId);
    }
    public ConsultarBeneficiariosRequest yNumeroSolicitud(String numeroSolicitud){
        this.numeroSolicitud=numeroSolicitud;
        return this;
    }
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("varlorNumeroID",numeroId).replace("valorNumeroSolicitud",numeroSolicitud);
    }
}
