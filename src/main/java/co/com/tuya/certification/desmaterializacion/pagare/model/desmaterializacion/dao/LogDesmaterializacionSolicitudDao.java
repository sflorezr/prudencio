package co.com.tuya.certification.desmaterializacion.pagare.model.desmaterializacion.dao;

import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import co.com.tuya.certification.desmaterializacion.pagare.model.desmaterializacion.dto.LogDesmaterializacionSolicitud;

public interface LogDesmaterializacionSolicitudDao {

	@SqlQuery("SELECT  CODDOC, NOMDOC, NOMAPP, CODSERV, NOMSERV, CODRE, DESRE FROM SUFSDAAU.ATT0120 WHERE NUMID = ? AND NUMDOC = ? ORDER BY FECHA DESC, HORA DESC LIMIT 1")
	LogDesmaterializacionSolicitud obtenerLogDesmaterializadoSolicitud(@Bind String numeroDocumento,@Bind String numeroSolicitud);
}
