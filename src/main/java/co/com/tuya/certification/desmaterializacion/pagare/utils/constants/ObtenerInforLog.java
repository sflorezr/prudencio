package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class ObtenerInforLog {
    public static final String ROOT_PATH_XML_CONSULTA_INFO_LOG="Envelope.Body.obtenerInformacionLogResponse";
    public static final String CAMPO_CODIGO_RESPUESTA="codigoRespuesta";
    public static final String CAMPO_DESCRIPCION_RESPUESTA="descripcionRespuesta";
    public static final String CAMPO_CODIGO_DOCUMENTO="codigoDocumentoDesmaterializado";
    public static final String CAMPO_NOMBRE_DOCUMENTO="nombreDocumentoDesmaterializado";
    public static final String CAMPO_APLICATIVO="nombreApliacionDesmaterializa";
    public static final String CAMPO_DOCUMENTO="numeroDocumentoDesmaterializado";
    public static final String CAMPO_CODIGO_RESPUESTA_FIRMA="codigoRespuestaFirma";
    public static final String CAMPO_DESCRIPCION_RESPUESTA_FIRMA="codigoRespuestaFirma";

}