package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPdfPagareService.*;
import co.com.tuya.certification.desmaterializacion.pagare.model.ObtenerPdfPagareServiceRequest;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ObtenerPdfPagareServiceStepDefinitions {
	@When("^I inquiry the service with numeroSolicitud:\"([^\"]*)\"$")
	public void iInquiryTheServiceWithNumeroSolicitud(String numSolicitud) {	    
		theActorInTheSpotlight().attemptsTo(
				Inquiry.promissoryNote(ObtenerPdfPagareServiceRequest.withNumeroSolicitud(numSolicitud).buildRequestForActor(theActorInTheSpotlight()))
				);
	}


	@Then("^I should see that the response the service ObtenerPdfPagare has codigoRespuesta:\"([^\"]*)\" and mensajeRespuesta:\"([^\"]*)\" and estado:\"([^\"]*)\" with fecha:\"([^\"]*)\" and idPagareDeceval:\"([^\"]*)\" and nombreOtorgante: \"([^\"]*)\" and numeroDocumentoOtorgante: \"([^\"]*)\" and tipoDocumento: \"([^\"]*)\"$")
	public void iShouldSeeThatTheResponseTheServiceObtenerPdfPagareHasCodigoRespuestaAndMensajeRespuestaAndEstadoWithFechaAndIdPagareDecevalAndNombreOtorganteAndNumeroDocumentoOtorganteAndTipoDocumento(String codigoRespuesta, String mensajeRespuesta, String estadoPagare, String fechaGrabacionPagare, String idPagareDeceval, String nombreOtorgante, String numeroDocumentoOtorgante, String tipoDocumento) {
	    theActorInTheSpotlight().attemptsTo(
	    		ValidateTheLastResponse.ofFields(FIELD_CODIGO_RESPUESTA_RESPONSE,FIELD_MENSAJE_RESPUESTA_RESPONSE,FIELD_ESTADO_PAGARE_RESPONSE,FIELD_FECHA_RESPONSE,FIELD_ID_PAGARE_RESPONSE,FIELD_NOMBRE_OTORGANTE_RESPONSE,FIELD_DOCUMENTO_OTORGANTE_RESPONSE,FIELD_TIPO_DOCUMENTO_RESPONSE).
	    		andTheRootPathIs(ROOT_PATH_XML_OBTENER_PDF_PAGARE).hasTheItems(codigoRespuesta, mensajeRespuesta, estadoPagare, fechaGrabacionPagare, idPagareDeceval, nombreOtorgante, numeroDocumentoOtorgante, tipoDocumento)
	    		);	    
	}
}
