package co.com.tuya.certification.desmaterializacion.pagare.model.desmaterializacion.dto;

public class LogDesmaterializacionSolicitud {
	private String CODDOC;
	private String NOMDOC;
	private String NOMAPP;
	private String CODSERV;
	private String NOMSERV;
	private String CODRE;
	private String DESRE;
	
	public LogDesmaterializacionSolicitud() {
		
	}
	
	public String getCODDOC() {
		return CODDOC;
	}
	public void setCODDOC(String cODDOC) {
		CODDOC = cODDOC;
	}
	public String getNOMDOC() {
		return NOMDOC;
	}
	public void setNOMDOC(String nOMDOC) {
		NOMDOC = nOMDOC;
	}
	public String getNOMAPP() {
		return NOMAPP;
	}
	public void setNOMAPP(String nOMAPP) {
		NOMAPP = nOMAPP;
	}
	public String getCODSERV() {
		return CODSERV;
	}
	public void setCODSERV(String cODSERV) {
		CODSERV = cODSERV;
	}
	public String getNOMSERV() {
		return NOMSERV;
	}
	public void setNOMSERV(String nOMSERV) {
		NOMSERV = nOMSERV;
	}
	public String getCODRE() {
		return CODRE;
	}
	public void setCODRE(String cODRE) {
		CODRE = cODRE;
	}
	public String getDESRE() {
		return DESRE;
	}
	public void setDESRE(String dESRE) {
		DESRE = dESRE;
	}

}
