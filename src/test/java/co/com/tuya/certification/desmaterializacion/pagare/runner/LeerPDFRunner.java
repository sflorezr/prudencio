package co.com.tuya.certification.desmaterializacion.pagare.runner;

import static cucumber.api.SnippetType.CAMELCASE;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(
        features = "src//test//resources//features//LeerPDF.feature",
        tags = {"@Carulla"},
        glue = {"co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions"},
        		//"co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookvalidarregistraduria"},
        snippets = CAMELCASE

)
@RunWith(CucumberWithSerenity.class)
public class LeerPDFRunner {

}
