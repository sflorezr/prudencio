Feature: Generar firma solicitud
  Como usuario quiero que al ejecutar este servicio se genere un archivo PDF con firma
  y Adicionalmente guarde registros en el AS400

  Scenario Outline: Generar firma solicitud PDF
    When Yo ejecuto servicio generarFirma con lo siguientes datos Aplicativo: "<nombreAplicacion>" , usuario: "<usuario>" , tipoDocumento: "<tipoDocumento>" , numeroDocumento: "<numeroDocumento>" , numeroSolicitud: "<numeroSolicitud>" y huella: "<huella>"
    Then Valido respuesta del servicio con codigoRespuesta: "<codigoRespuesta>" y mensajeRespuesta: "<mensajeRespuesta>"
    And Consulto archivo ATT0120 con datos numeroDocumento: "<numeroDocumento>" y numeroSolicitud: "<numeroSolicitud>"
    Then Yo verifico codigoDocumento: "<codigoDocumento>" , nombreDocumento: "<nombreDocumento>" , nombreAplicacion: "<nombreAplicacion>" , codigoServicio: "<codigoServicio>" , nombreServicio: "<nombreServicio>" , codigoRespuesta: "<codigoRespuesta>" , mensajeRespuesta: "<mensajeRespuesta>"

    Examples: 
      | aplicativo | usuario | tipoDocumento | numeroDocumento | numeroSolicitud | nombreAplicacion | codigoRespuesta | mensajeRespuesta                 | codigoDocumento | nombreDocumento | codigoServicio | nombreServicio      |
      | HOMINI     | QVI1SLO |             1 |        35465844 |       700502318 | PRUEBAHUELLA     |            1000 | Documento Generado Correctamente |               1 | SOLICITUD       |              1 | Transacción exitosa |
