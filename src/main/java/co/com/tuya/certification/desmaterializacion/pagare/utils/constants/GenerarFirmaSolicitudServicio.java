package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class GenerarFirmaSolicitudServicio {
	
	public static final String ROOT_PATH_XML_GENERAR_FIRMA = "Envelope.Body.generarFirmaResponse";
	public static final String CODIGO_RESPUESTA = "codigoRespuesta";
	public static final String MENSAJE_RESPUESTA = "mensajeRespuesta";
	
	
	private GenerarFirmaSolicitudServicio() {
		
	}

}
