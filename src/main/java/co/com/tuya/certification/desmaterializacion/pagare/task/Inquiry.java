package co.com.tuya.certification.desmaterializacion.pagare.task;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.Post;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import net.serenitybdd.markers.IsSilent;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
//import net.serenitybdd.screenplay.rest.interactions.Post;

public class Inquiry implements Task,IsSilent {
    private String bodyRequest;

    public Inquiry(String bodyRequest) {
        this.bodyRequest = bodyRequest;
    }

    public static Inquiry promissoryNote(String bodyRequest) {
        return instrumented(Inquiry.class, bodyRequest);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to("").with(request -> request.contentType("application/soap+xml;charset=UTF-8")
                        .body(bodyRequest).relaxedHTTPSValidation())
        );
         actor.should(
                seeThatResponse("Prueba de estado",
                         response -> response.statusCode(200).extract().response()));
    }
}
