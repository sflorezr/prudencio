package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.FirmarPagareAs400;
import co.com.tuya.certification.desmaterializacion.pagare.questions.TheLastResponseOfFirmaPagare;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class ValidateAs400EvidenceFirmaPagare implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        FirmarPagareAs400 firmarPagareAs400 = actor.recall("FirmaPagareAs400");
        actor.should(
                seeThat(TheLastResponseOfFirmaPagare.theField("codigoRespuesta"), is(equalTo(firmarPagareAs400.getCodigoDeRespuesta()))),
                seeThat(TheLastResponseOfFirmaPagare.theField("usuarioRed"), is(equalTo(firmarPagareAs400.getUsuarioAsesorTuya()))),
                seeThat(TheLastResponseOfFirmaPagare.theField("catt"), is(equalTo(firmarPagareAs400.getCattOrigen()))),
                seeThat(TheLastResponseOfFirmaPagare.theField("idPagareDeceval"), is(equalTo(firmarPagareAs400.getIdPagareDeceval()))));
    }
}
