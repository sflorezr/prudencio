package co.com.tuya.certification.desmaterializacion.pagare.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Step;

import static co.com.tuya.certification.desmaterializacion.pagare.model.builder.ConnectionBuilder.createNewConnectionTo;

public class CreateConnection implements Interaction {
	private String userName;
	private String password;
	private String url;

	public CreateConnection(String url) {
		this.url = url;
	}

	public CreateConnection withUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public CreateConnection andPassword(String password) {
		this.password = password;
		return this;
	}

	public static CreateConnection toAs400(String url) {
		return Tasks.instrumented(CreateConnection.class, url);
	}

	@Step("{0} creates a new connection to #url with userName:#userName and password:#password")
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.remember("ConexionAs400", createNewConnectionTo(url).withUser(userName).andPassword(password).build());
	}

}
