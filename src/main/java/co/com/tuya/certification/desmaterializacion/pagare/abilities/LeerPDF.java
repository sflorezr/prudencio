package co.com.tuya.certification.desmaterializacion.pagare.abilities;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.ActorNoPuedeLeerPDF;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

public class LeerPDF implements Ability{
    private String pathXmlFile;
    private static final Logger logger = Logger.getLogger(ReadXml.class.getName());
	
    public LeerPDF(String pathXmlFile) {
		this.pathXmlFile = pathXmlFile;
	}
    
    public static LeerPDF fromPath(String pathPDF) {
    	return new LeerPDF(pathPDF);
    }
    	
    public static LeerPDF as (Actor actor) {
    	if(actor.abilityTo(LeerPDF.class)==null) {
    		throw new ActorNoPuedeLeerPDF(actor.getName());
    	}
    	return actor.abilityTo(LeerPDF.class);
    }
    
    public String[] resolve() {
    	String PDFLeido[] = null;
    	try {
			PDDocument document = PDDocument.load(new File(pathXmlFile));
	        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
	        stripper.setSortByPosition(true);

	        PDFTextStripper tStripper = new PDFTextStripper();

	        String pdfFileInText = tStripper.getText(document);

	        PDFLeido = pdfFileInText.split("\\r?\\n");	
	       /* for (int i = 0; i < PDFLeido.length; i++) {
	        	System.out.println(PDFLeido[i]);
			}*/
		} catch (InvalidPasswordException e) {
			logger.error("Clave invalida");
		} catch (IOException e) {
			logger.error("No pudo abrir el archivo "+pathXmlFile);
		}
		return PDFLeido;    
    }
}
