package co.com.tuya.certification.desmaterializacion.pagare.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ValidacionPorHuella {
    private String identificacion;
    private String estadoValidacion;
    private String fecha;
    private String hora;
    private String codigoProceso;

    public ValidacionPorHuella(String identificacion) {
    	Date date = new Date();
    	DateFormat hourFormat = new SimpleDateFormat("HHmmss");
    	this.hora=hourFormat.format(date);
    	DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    	this.fecha=dateFormat.format(date);
        this.identificacion = identificacion;               
    }

    public static ValidacionPorHuella withTipoIdentificacion(String tipoIdentificacion){
        return new ValidacionPorHuella(tipoIdentificacion);
    }
    public ValidacionPorHuella andEstadoValidacion(String estadoValidacion) {
    	switch (estadoValidacion) {
		case "VCX":
			codigoProceso="0";
			break;
		case "VLM":
			codigoProceso="10";
			break;
		case "NAE":
			codigoProceso="1";
			break;			
		default:
			codigoProceso="0";
			break;
		}  
    	this.estadoValidacion=estadoValidacion;
    	return this;
    }
    
    public ValidacionPorHuella andExcede(String exceTiempo){
    	if (exceTiempo.equals("N")) {
    		this.hora="010000";
    	}    	
		return this;        
    }    

    public ValidacionPorHuella andFechaActual(String fechaActual){
    	if (fechaActual.equals("N")) {
    		this.fecha="20180101";
    	}    	
		return this;        
    } 
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valorNumeroDocumento", identificacion).replace("valorFecha", fecha).replace("valorHora",hora).replace("valorEstadoValidacion", estadoValidacion).replace("valorcodigoProceso", codigoProceso);
    }


}
