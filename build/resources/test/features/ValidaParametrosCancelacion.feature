# language: es
Característica: Validar parametros para Cancelacion masiva

  Escenario: Validar parametros SA0050
  A50NPR= PTT0095A

    Cuando Consulto en el archivo SA0050 con filtro A50NPR=PTT0181
    Entonces Valido respuesta de parametros para respuestas del servicio SOAP
      | 1 | 60       | NUMERO DE DIAS PARA VALIDAR LAS CANCELACIONES |
      | 2 | 900      | CONCEPTO DE CANCELACION PARA SIIL01           |
      | 3 | 20190301 | SELECCIONAR SIIL01 DESDE LA FECHA DE APERTURA |
      | 4 | Z        | ESTADO DE CANCELACION A GRABAR EN ATT0119     |
      | 5 | 06       | NUEVO ESTADO A GRABAR EN ATT0119              |
      | 6 | N        | MARCA CUANDO NO EXISTE EN ATT0119             |
      | 7 | E        | MARCA CUANDO SE GRABA EXITOSO EN ATT0119      |
      | 8 | X        | MARCA CUANDO NO SE PUEDE GRABAR EN ATT0119    |