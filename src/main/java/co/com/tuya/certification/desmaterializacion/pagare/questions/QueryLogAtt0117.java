package co.com.tuya.certification.desmaterializacion.pagare.questions;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.QueryAs400;
import co.com.tuya.certification.desmaterializacion.pagare.model.dto.FirmarPagareAs400;
import co.com.tuya.certification.desmaterializacion.pagare.model.dto.QueryAtt0117;
import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class QueryLogAtt0117 extends QueryAs400 implements Question<List<String>> {
	private String query;
	private static List<String> as400 = new ArrayList<>();
	private static final Logger LOGGER = Logger.getLogger(QueryLogAtt0117.class.getName());
	private static ResultSet resultSet;
	private static FirmarPagareAs400 firmarPagareAs400;

	public QueryLogAtt0117(QueryAtt0117 queryAtt0117, String query) {
		super(queryAtt0117);
		this.query = query;
	}

	@Override
	public List<String> answeredBy(Actor actor) {
		Connection connectionToAs400 = actor.recall("ConexionAs400");
		resultSet = prepareStatement(connectionToAs400, query);
		actor.remember("ResultadosAs400", resultSet);
		return dataAs400(actor, "A0117NUSOL", "A0117TIPID", "A0117NUMID", "A0117IDASE", "A0117CATT", "A0117CODRE",
				"A0117DESRE");
	}

	private <T extends Actor> List<String> dataAs400(T actor, String... nameColumn) {
		List<String> valores = asList(nameColumn);
		try {
			resultSet.next();
			valores.forEach(valor -> as400.add(getcolumnValue(valor)));
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new BackendException();
		}
		firmarPagareAs400 = FirmarPagareAs400.withNumeroDeSolicitud(getcolumnValue("A0117NUSOL"))
				.andTipoDocumento(getcolumnValue("A0117TIPID")).andNumeroDeDocumento(getcolumnValue("A0117NUMID"))
				.andCattOrigen(getcolumnValue("A0117CATT")).andUsuarioAsesorTuya(getcolumnValue("A0117IDASE"))
				.andCodigoDeRespuesta(getcolumnValue("A0117CODRE"))
				.andDescripcionRespuesta(getcolumnValue("A0117DESRE")).andIdPagareDeceval(getcolumnValue("A0117IDPAG"));
		Serenity.recordReportData().withTitle("AS400 Evidence").andContents(firmarPagareAs400.toString());
		actor.remember("FirmaPagareAs400", firmarPagareAs400);
		return as400;
	}

	private String getcolumnValue(String nameColum) {
		try {
			return resultSet.getString(nameColum).trim();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new BackendException("no se pudo leer el valor del campo:" + nameColum);
		}

	}

}
