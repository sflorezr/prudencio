Feature: Consulta beneficiarios

  Scenario Outline: Consultar beneficiarios de una solicitud
    When Yo ejecuto servicio ConsultarBeneficiarios con lo siguientes datos numeroDocumento: "<numeroDocumento>" , numeroSolicitud: "<numeroSolicitud>"
    Then Valido respuesta del servicio ConsultarBeneficiarios con codigoRespuesta: "<codigoRespuesta>", mensajeRespuesta: "<mensajeRespuesta>", numeroBeneficiario: "<numeroBeneficiario>", apellidosBeneficiarios: "<apellidoBeneficiario>", cedulaBeneficiario: "<cedulaBeneficiario>"

    Examples:
      | numeroDocumento | numeroSolicitud | codigoRespuesta | mensajeRespuesta                                                                   | numeroBeneficiario | apellidoBeneficiario   | cedulaBeneficiario |
      | 1001670284      | 700519152       | 2000            | Solicitud exitosa.                                                                 | 0                  | Malagón florez         | 1090442524         |
      | 1001670284      | 700519152       | 2000            | Solicitud exitosa.                                                                 | 1                  | Apellidos Beneficiario | 654321             |
      | 1001670284      | 700519152       | 2000            | Solicitud exitosa.                                                                 | 2                  | Florez Rangel          | 1152434257         |
      | 1001670284      |                 | 2006            | Error de Validación en Request \n- El campo NumeroSolicitud no es válido           |                    |                        |                    |
      | 1001670284      | 123             | 2005            | Solicitud sin beneficiarios.                                                       |                    |                        |                    |
      |                 | 123             | 2006            | Error de Validación en Request \n- El campo NumeroID - Cédula Titular no es válido |                    |                        |                    |