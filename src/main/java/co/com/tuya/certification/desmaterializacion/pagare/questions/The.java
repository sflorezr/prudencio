package co.com.tuya.certification.desmaterializacion.pagare.questions;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.QueryAtt0117;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_ATT0117;

public class The {
	public static QueryLogAtt0117 logAtt0117(QueryAtt0117 queryAtt0117) {
		return new QueryLogAtt0117(queryAtt0117, QUERY_ATT0117);
	}
}
