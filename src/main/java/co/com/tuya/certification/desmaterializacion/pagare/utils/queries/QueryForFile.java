package co.com.tuya.certification.desmaterializacion.pagare.utils.queries;

import co.com.tuya.certification.desmaterializacion.pagare.utils.managers.PropertiesManager;

public class QueryForFile {
    public static final String USER_AS400 = PropertiesManager.QUERY_AS400.getString("user");
    public static final String PASSWORD_AS400 = PropertiesManager.QUERY_AS400.getString("password");
    public static final String URL_DATABASE = PropertiesManager.QUERY_AS400.getString("url_database");
	public static final String QUERY_ATT0117 = PropertiesManager.QUERY_AS400.getString("LOG.ATT0117");
	public static final String QUERY_ATT0119 = PropertiesManager.QUERY_AS400.getString("LOG.ATT0119");
	public static final String QUERY_SA0050 = PropertiesManager.QUERY_AS400.getString("PR.SA0050");
	public static final String QUERY_ATT0041 = PropertiesManager.QUERY_AS400.getString("LOG.ATT0041");
	public static final String QUERY_ATT0120 = PropertiesManager.QUERY_AS400.getString("LOG.ATT0120");

    private QueryForFile() {
    }
}
