package co.com.tuya.certification.desmaterializacion.pagare.questions;

import java.sql.SQLException;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LastResponseAs400 implements Question<String> {

	private String field;
	
	public LastResponseAs400(String field) {
		this.field = field;
	}

	@Override
	public String answeredBy(Actor actor) {

		try {
			return Consult.getLastResponse().getString(field).trim();
		} catch (SQLException e) {	
			return ("vacio");
		}
	}

	public static LastResponseAs400 Consulta(String field) {
		return new LastResponseAs400(field);
	}

}
