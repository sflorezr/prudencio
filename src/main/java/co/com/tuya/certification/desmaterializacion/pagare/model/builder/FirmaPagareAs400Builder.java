package co.com.tuya.certification.desmaterializacion.pagare.model.builder;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.FirmarPagareAs400;

public class FirmaPagareAs400Builder {
	private String numeroDeSolicitud;
	private String tipoDocumento;
	private String numeroDeDocumento;
	private String usuarioAsesorTuya;
	private String cattOrigen;
	private String codigoDeRespuesta;
	private String descripcionRespuesta;

	public FirmaPagareAs400Builder(String numeroDeSolicitud) {
		this.numeroDeSolicitud = numeroDeSolicitud;
	}

	public static FirmaPagareAs400Builder withNumeroDeSolicitud(String numeroDeSolicitud) {
		return new FirmaPagareAs400Builder(numeroDeSolicitud);
	}

	public FirmaPagareAs400Builder andTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
		return this;
	}

	public FirmaPagareAs400Builder andNumeroDeDocumento(String numeroDeDocumento) {
		this.numeroDeDocumento = numeroDeDocumento;
		return this;
	}

	public FirmaPagareAs400Builder andUsuarioAsesorTuya(String usuarioAsesorTuya) {
		this.usuarioAsesorTuya = usuarioAsesorTuya;
		return this;
	}

	public FirmaPagareAs400Builder andCattOrigen(String cattOrigen) {
		this.cattOrigen = cattOrigen;
		return this;
	}

	public FirmaPagareAs400Builder andCodigoDeRespuesta(String codigoDeRespuesta) {
		this.codigoDeRespuesta = codigoDeRespuesta;
		return this;
	}

	public FirmaPagareAs400Builder andDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
		return this;
	}

	public String getNumeroDeSolicitud() {
		return numeroDeSolicitud;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public String getNumeroDeDocumento() {
		return numeroDeDocumento;
	}

	public String getUsuarioAsesorTuya() {
		return usuarioAsesorTuya;
	}

	public String getCattOrigen() {
		return cattOrigen;
	}

	public String getCodigoDeRespuesta() {
		return codigoDeRespuesta;
	}

	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

}