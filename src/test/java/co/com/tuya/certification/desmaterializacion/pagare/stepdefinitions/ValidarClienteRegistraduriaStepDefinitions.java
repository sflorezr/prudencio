package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;


import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.FIELD_CAPTURA_HUELLA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.FIELD_CODIGO_RESPUESTA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.FIELD_ESTADO_VALIDACION_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.FIELD_MENSAJE_RESPUESTA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.FIELD_OBSERVACION_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ValidarClienteRegistraduria.ROOT_PATH_XML_VALIDAR_CLIENTE_REGISTRADURIA;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.PASSWORD_AS400;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_ATT0041;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_SA0050;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.URL_DATABASE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.USER_AS400;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import java.util.List;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.CreateConnection;
import co.com.tuya.certification.desmaterializacion.pagare.model.ValidacionPorHuella;
import co.com.tuya.certification.desmaterializacion.pagare.model.ValidarClienteRegistraduria;
import co.com.tuya.certification.desmaterializacion.pagare.questions.Consult;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponseOfAs400;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;

public class ValidarClienteRegistraduriaStepDefinitions {
    private static final String USER_HOMINI = "USER HOMINI";
	@Cuando("^Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:\"([^\"]*)\" y numeroIdentificacion:\"([^\"]*)\" y excedeTiempo:\"([^\"]*)\" y estadoValidacion:\"([^\"]*)\" y fechaActual:\"([^\"]*)\"$")	
	public void consultoElServicioValidarClienteRegistraduriaConTipoDocumentoYNumeroIdentificacionYExcedeTiempoYEstadoValidacionYFechaActual(String tipoIdentificacion, String identificacion,String excedeTiempo,String estadoValidacion,String fechaActual) {
	if (!estadoValidacion.equals("0")) {	
		theActorCalled("checho").entersTheScene();
		theActorInTheSpotlight().attemptsTo(
				Inquiry.promissoryNote(ValidacionPorHuella.withTipoIdentificacion(identificacion).andEstadoValidacion(estadoValidacion).andExcede(excedeTiempo).buildRequestForActor(theActorInTheSpotlight())));
	}
	theActorCalled(USER_HOMINI).entersTheScene();
		theActorInTheSpotlight().attemptsTo(
				Inquiry.promissoryNote(ValidarClienteRegistraduria.withTipoIdentificacion(tipoIdentificacion).andIdentificacion(identificacion).buildRequestForActor(theActorInTheSpotlight()))
				);
	}


	@Entonces("^Valido respuesta del SOAP con codigoRespuesta:\"([^\"]*)\" y mensajeRespuesta:\"([^\"]*)\" y observacion:\"([^\"]*)\" with capturaHuella:\"([^\"]*)\" y estadoValidacion:\"([^\"]*)\"$")	
	public void validoRespuestaDelSOAPConCodigoRespuestaYMensajeRespuestaYObservacionWithCapturaHuellaYEstadoValidacion(String codigoRespuesta,String mensajeRespuesta, String observacion, String capturarHuella, String estadoValidacion) {
	    theActorInTheSpotlight().attemptsTo(
	    		ValidateTheLastResponse.ofFields(FIELD_CODIGO_RESPUESTA_RESPONSE,FIELD_MENSAJE_RESPUESTA_RESPONSE,FIELD_OBSERVACION_RESPONSE,FIELD_CAPTURA_HUELLA_RESPONSE,FIELD_ESTADO_VALIDACION_RESPONSE).
	    		andTheRootPathIs(ROOT_PATH_XML_VALIDAR_CLIENTE_REGISTRADURIA).hasTheItems(codigoRespuesta,mensajeRespuesta, observacion, capturarHuella,estadoValidacion)
	    		);
	    
	}

	@Cuando("^Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: \"([^\"]*)\" y numeroIdentificacion: \"([^\"]*)\"$")
	public void consultoAS400EnElArchivoATT0041ConAndTipoIdentificacionYNumeroIdentificacion(String tipoIdentificacion, String numeroIdentificacion) {
		theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                Consult.logWithQuery(QUERY_ATT0041).withParameters(tipoIdentificacion, numeroIdentificacion));
	}

	@Entonces("^valido respuesta del archivo con estadoValidacion:\"([^\"]*)\"$")
	public void validoRespuestaDelArchivoConEstadoValidacion(String estadoValidacion) {
		if (!estadoValidacion.equals("0")) {
		theActorInTheSpotlight().attemptsTo(ValidateTheLastResponseOfAs400.ofFields("A0041DRT").hasTheItems(estadoValidacion));
		}
	}
	
	@Cuando("^Consulto en el archivo SA0050 con filtro A50NPR=PTT0095A$")
	public void consultoEnElArchivoSA0050ConFiltroA50NPRPTT0095A() {
		theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                Consult.logWithQuery(QUERY_SA0050).withParameters("PTT0095A"));
	}


	@Cuando("^Consulto en el archivo SA0050 con filtro A50NPR=PTT0181$")
	public void consultoEnElArchivoSA0050ConFiltroA50NPRPTT0181() {
		theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
				Consult.logWithQuery(QUERY_SA0050).withParameters("PTT0181"));
	}

	@Cuando("^Valido respuesta de parametros para respuestas del servicio SOAP$")
	public void validoRespuestaDeParametrosParaRespuestasDelServicioSOAP(List<List<String>> Tabla) {
			Tabla.forEach(tabla->theActorInTheSpotlight().attemptsTo(ValidateTheLastResponseOfAs400.ofFields("A50TIP","A50CON","A50EQU").hasTheItems(tabla.get(0),tabla.get(1),tabla.get(2))));
		
	}


}

