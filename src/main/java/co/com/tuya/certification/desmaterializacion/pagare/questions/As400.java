package co.com.tuya.certification.desmaterializacion.pagare.questions;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

public class As400 implements Question<String> {
	private static Logger LOGGER = Logger.getLogger(As400.class.getName());
	private String column;

	public As400(String column) {
		this.column = column;
	}

	public static <T> T columnValue(String column) {
		return (T) new As400(column);
	}

	@Override
	public String answeredBy(Actor actor) {
		ResultSet resultSet = actor.recall("ResultadosAs400");
		try {
			resultSet.next();
			return resultSet.getString(column).trim();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new BackendException("no se pudo consultar el valor de la columna:" + column);
		}
	}
}
