package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.CreateConnection;
import co.com.tuya.certification.desmaterializacion.pagare.model.dto.QueryAtt0117;
import co.com.tuya.certification.desmaterializacion.pagare.questions.The;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.hasItems;

public class ConsultOld implements Task {

	private QueryAtt0117 queryAtt0117;

	public ConsultOld(QueryAtt0117 queryAtt0117) {
		this.queryAtt0117 = queryAtt0117;
	}

	public static ConsultOld log(QueryAtt0117 queryAtt0117) {
		return Tasks.instrumented(ConsultOld.class, queryAtt0117);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(CreateConnection.toAs400("jdbc:as400://10.169.104.172:23").withUserName("QVI2SLO")
				.andPassword("QVI2SLO+sl"));
		actor.should(seeThat(The.logAtt0117(queryAtt0117),
				hasItems(Integer.toString(queryAtt0117.getNumeroDeSolicitud()), queryAtt0117.getTipoDocumento(),
						queryAtt0117.getNumeroDocumento(), queryAtt0117.getCodigoDeRespuesta(),
						queryAtt0117.getDescripcionRespuesta())

		));
	}
}
