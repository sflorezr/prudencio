package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class ConsultaBeneficiarios {
    public static final String ROOT_PATH_XML_CONSULTA_BENEFICIARIOS="Envelope.Body.ConsultaBeneficiariosResponse.ConsultaBeneficiariosResult";
    public static final String FIELD_APELLIDO="ApellidosBeneficiario";
    public static final String FIELD_DOCUMENTO="BeneficiarioID";
    public static final String FIELD_RESPUESTA="Respuesta";
    public static final String FIELD_CODIGO="CodigoRespuesta";
}
