package co.com.tuya.certification.desmaterializacion.pagare.model.dto;

public class GenerarPagareResponse {
    private  String codigoRespuesta;
    private  String mensajeRespuesta;

    public GenerarPagareResponse(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }

    public static GenerarPagareResponse withCodigoRespuesta(String codigoRespuesta){
        return new GenerarPagareResponse(codigoRespuesta);
    }

    public GenerarPagareResponse andMensajeRespuesta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
        return this;
    }

    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }
}
