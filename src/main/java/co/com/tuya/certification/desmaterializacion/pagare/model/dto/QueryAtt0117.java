package co.com.tuya.certification.desmaterializacion.pagare.model.dto;

public class QueryAtt0117 {
	private int numeroDeSolicitud;
	private String identificadorTrama;
	private String descripcionRespuesta;
	private String codigoDeRespuesta;
	private String nombreDelServicio;
	private String cattOrigen;
	private String tipoDocumento;
	private String numeroDocumento;
	private String usuarioAsesorTuya;

	public String getUsuarioAsesorTuya() {
		return usuarioAsesorTuya;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public String getCattOrigen() {
		return cattOrigen;
	}

	public String getNombreDelServicio() {
		return nombreDelServicio;
	}

	public int getNumeroDeSolicitud() {
		return numeroDeSolicitud;
	}

	public String getIdentificadorTrama() {
		return identificadorTrama;
	}

	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	public String getCodigoDeRespuesta() {
		return codigoDeRespuesta;
	}
}
