package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookvalidarregistraduria;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.QueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.*;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class ValidarClienteRegistraduria {
    private static final String USER_HOMINI = "USER HOMINI";
    @Before
    public void setup(){
        setTheStage(new OnlineCast());
        theActorCalled(USER_HOMINI).whoCan(CallAnApi.at("https://10.169.104.206:7083/intf/fw/ChannelAdapter/V2.0"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_VALIDAR_CLIENTE_REGISTRADURIA));
        theActorInTheSpotlight().whoCan(QueryTheDatabase.as400());
        theActorCalled("checho").whoCan(CallAnApi.at("https://10.169.104.206:7083/intf/fw/ChannelAdapter/V2.0"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_VALIDACION_HUELLA));


    }
    @After
    public void finishSetup(){
        drawTheCurtain();
    }
}
