package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import co.com.tuya.certification.desmaterializacion.pagare.questions.CodigoRespuesta;
import co.com.tuya.certification.desmaterializacion.pagare.task.AnulacionMasiva;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class AnulacionMasivaDefinitions {
    @Cuando("^Ejecuto servicio AnulacionMasica con Esquema: \"([^\"]*)\" idPagare: \"([^\"]*)\" idSolicitud: \"([^\"]*)\"$")
    public void ejecutoServicioAnulacionMasicaConEsquemaIdPagareIdSolicitud(String esquema, String idPagare, String idSolicitud) {
        theActorInTheSpotlight().attemptsTo(AnulacionMasiva.conCampos(idPagare,idSolicitud));
    }

    @Entonces("^Valido respuesta del servicio AnulacionMasiva \"([^\"]*)\"$")
    public void validoRespuestaDelServicioAnulacionMasiva(String codigo) {
        theActorInTheSpotlight().should(seeThat("El codigo de respuesta", CodigoRespuesta.fue(),equalTo(Integer.parseInt(codigo))));
    }

}
