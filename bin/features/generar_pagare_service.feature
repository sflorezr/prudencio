Feature: Sign promissory note
  As a user in Homini
  I want to sign the promissory note
  to get my credit line

  Scenario Outline: Sign the promissory note of a Customer already
    When I sign the promissory note with numeroSolicitud: "<numeroSolicitud>" with my clave:"<clave>" with my document with "<tipoDocumento>" and numeroDocumento:"<numeroDocumento>" with my fingerprint:"<contenido>"
    Then I should see the responseCode:"<codigoDeRespuesta>" with responseMessage:"<mensajeRespuesta>"
    When I inquiry the AS400 whith numeroSolicitud: "<numeroSolicitud>" and tipoIdentificacion: "<tipoDocumento>" and numeroIdentificacion: "<numeroDocumento>" and codigoRespuesta: "<codigoDeRespuesta>" and mensajeRespuesta: "<mensajeRespuestaAs400>"
    Then I should see that the response has numeroSolicitud: "<numeroSolicitud>" and tipoIdentificacion: "<tipoDocumento>" and numeroIdentificacion: "<numeroDocumento>" and codigoRespuesta: "<codigoDeRespuesta>" and mensajeRespuesta: "<mensajeRespuestaAs400>"


    Examples:
      | numeroSolicitud | clave    | tipoDocumento | numeroDocumento | contenido                                                         | codigoDeRespuesta | mensajeRespuesta                             | mensajeRespuestaAs400 |
      | 700509080       | hazrj1F* | 1             | 19091738        | MDA3NgECAAABaAAAAKQAAO123456789ABC123451234567890ADCDADSDFR1234AB | SDL.SE.0000       | Exitoso-El pagare se ha firmado exitosamente | SDL.SE.0000: Exitoso. |
