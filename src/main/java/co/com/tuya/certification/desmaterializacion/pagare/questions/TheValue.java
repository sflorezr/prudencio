package co.com.tuya.certification.desmaterializacion.pagare.questions;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.rest.questions.LastResponse;

import java.sql.SQLException;

@Subject("the value of the field #field")
public class TheValue implements Question<String> {
    private String field;
    private String roothPath;

    public TheValue withRoothPath(String roothPath) {
        this.roothPath = roothPath;
        return this;
    }

    public TheValue(String field) {
        this.field = field;
    }

    public static TheValue ofField(String field) {
        return new TheValue(field);
    }

    @Override
    public String answeredBy(Actor actor) {
        return LastResponse.received().answeredBy(actor).xmlPath().setRoot(roothPath).getString(field).trim();
    }
    public String AnsweredByAs400(Actor actor){
        try {
            return Consult.getLastResponse().getString(field);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new BackendException("REVENTE DURISIMO");
        }
    }
}
