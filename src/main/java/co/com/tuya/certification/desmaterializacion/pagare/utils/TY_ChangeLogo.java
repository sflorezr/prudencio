package co.com.tuya.certification.desmaterializacion.pagare.utils;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;


public class TY_ChangeLogo {
	public void changeLogo() {
		
		String strFileOrigen = "src/test/resources/images/serenity-logo.png";
		String strPathDestino = "target/site/serenity/images"; 
		
		Path ptPathOrigen = Paths.get(strFileOrigen);

		if (Files.exists(ptPathOrigen)) {
			
			Path ptPathDestino = Paths.get(strPathDestino);
			if (Files.exists(ptPathDestino)) {

				File flFileDestino = new File(strPathDestino + "/serenity-logo.png");
				File flFileOrigen = new File(strFileOrigen);

				long lngPesoOrigen = flFileOrigen.length();
				long lngPesoDestino = flFileDestino.length();
				
				if (lngPesoOrigen!=lngPesoDestino){
					File flPathDestino = new File(strPathDestino);
					try 
					{
						FileUtils.copyFileToDirectory(flFileOrigen, flPathDestino);											
					}
					catch (Exception e)
					{
						e.printStackTrace();
						
					}
				}
			}
		}
		
	}

}
