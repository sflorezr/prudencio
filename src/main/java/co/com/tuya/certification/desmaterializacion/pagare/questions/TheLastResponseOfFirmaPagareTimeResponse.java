package co.com.tuya.certification.desmaterializacion.pagare.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.rest.questions.LastResponse;

import java.util.concurrent.TimeUnit;


@Subject("the last response should be less than 9000 miliseconds")
public class TheLastResponseOfFirmaPagareTimeResponse implements Question<Long> {
    private TimeUnit responseTime;
	public TheLastResponseOfFirmaPagareTimeResponse(TimeUnit responseTime) {
		this.responseTime = responseTime;
	}

    @Override
    public Long answeredBy(Actor actor) {
        return LastResponse.received().answeredBy(actor).getTimeIn(responseTime);
    }

}
