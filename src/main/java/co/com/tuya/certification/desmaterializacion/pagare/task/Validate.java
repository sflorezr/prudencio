package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.GenerarPagareResponse;
import net.serenitybdd.screenplay.Task;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Validate {
	private Validate() {
	}

	public static Task as400Evidence() {
		return instrumented(ValidateAs400EvidenceFirmaPagare.class);
	}

	public static Task theLastResponseOfFirmaPagare(GenerarPagareResponse generarPagareResponse) {
		return instrumented(ValidateTheLastResponseOfFirmaPagare.class, generarPagareResponse);
	}

	public static Task theLastREsponseOfObtenerPagare() {
		return instrumented(ValidateTheLastResponseOfObtenerPagare.class);
	}

}
