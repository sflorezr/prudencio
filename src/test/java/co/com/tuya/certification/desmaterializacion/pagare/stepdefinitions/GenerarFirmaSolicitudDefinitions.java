package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.GenerarFirmaSolicitudServicio.CODIGO_RESPUESTA;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.GenerarFirmaSolicitudServicio.MENSAJE_RESPUESTA;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.GenerarFirmaSolicitudServicio.ROOT_PATH_XML_GENERAR_FIRMA;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.PASSWORD_AS400;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_ATT0120;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.URL_DATABASE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.USER_AS400;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.CreateConnection;
import co.com.tuya.certification.desmaterializacion.pagare.model.GenerarFirmaSolicitudRequest;
import co.com.tuya.certification.desmaterializacion.pagare.questions.Consult;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponseOfAs400;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GenerarFirmaSolicitudDefinitions {
	
	@When("^Yo ejecuto servicio generarFirma con lo siguientes datos Aplicativo: \"([^\"]*)\" , usuario: \"([^\"]*)\" , tipoDocumento: \"([^\"]*)\" , numeroDocumento: \"([^\"]*)\" , numeroSolicitud: \"([^\"]*)\" y huella: \"([^\"]*)\"$")
	public void yoEjecutoServicioGenerarFirmaConLoSiguientesDatosAplicativoUsuarioTipoDocumentoNumeroDocumentoNumeroSolicitudYHuella(String Aplicativo, String usuario, String tipoDocumento, String numeroDocumento, String numeroSolicitud, String huella) {
		theActorInTheSpotlight().attemptsTo(
				Inquiry.promissoryNote(GenerarFirmaSolicitudRequest.conNumeroDocumento(numeroDocumento).yAplicativo(Aplicativo).yUsuario(usuario).yTipoIdentificacion(tipoDocumento).yNumeroSolicitud(numeroSolicitud).buildRequestForActor(theActorInTheSpotlight()))
				);
	    
	}


	@Then("^Valido respuesta del servicio con codigoRespuesta: \"([^\"]*)\" y mensajeRespuesta: \"([^\"]*)\"$")
	public void validoRespuestaDelServicioConCodigoRespuestaYMensajeRespuesta(String codigoRespuesta, String mensajeRespuesta) {
        theActorInTheSpotlight().attemptsTo(
                ValidateTheLastResponse.ofFields(CODIGO_RESPUESTA,MENSAJE_RESPUESTA).andTheRootPathIs(ROOT_PATH_XML_GENERAR_FIRMA).hasTheItems(codigoRespuesta, mensajeRespuesta)
        );	    
	}

	@Then("^Consulto archivo ATT0120 con datos numeroDocumento: \"([^\"]*)\" y numeroSolicitud: \"([^\"]*)\"$")
	public void consultoArchivoATT0120ConDatosNumeroDocumentoYNumeroSolicitud(String numeroDocumento, String numeroSolicitud) {
		theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                Consult.logWithQuery(QUERY_ATT0120).withParameters(numeroDocumento, numeroSolicitud));
	
	}

	@Then("^Yo verifico codigoDocumento: \"([^\"]*)\" , nombreDocumento: \"([^\"]*)\" , nombreAplicacion: \"([^\"]*)\" , codigoServicio: \"([^\"]*)\" , nombreServicio: \"([^\"]*)\" , codigoRespuesta: \"([^\"]*)\" , mensajeRespuesta: \"([^\"]*)\"$")
	public void yoVerificoCodigoDocumentoNombreDocumentoNombreAplicacionCodigoServicioNombreServicioCodigoRespuestaMensajeRespuesta(String codigoDocumento, String nombreDocumento, String nombreAplicacion, String codigoServicio, String nombreServicio, String codigoRespuesta, String mensajeRespuesta) {
		theActorInTheSpotlight().attemptsTo(ValidateTheLastResponseOfAs400.ofFields("CODDOC", "NOMDOC", "NOMAPP", "CODSERV", "NOMSERV", "CODRE", "DESRE" ).hasTheItems(codigoDocumento, nombreDocumento, nombreAplicacion, codigoServicio, nombreServicio, codigoRespuesta, mensajeRespuesta));
	}

}
