package co.com.tuya.certification.desmaterializacion.pagare.questions;

import java.util.concurrent.TimeUnit;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Question;


public class TheLastResponseOfFirmaPagare  {
    public static TheLastResponseOfFirmaPagareField theField(String xmlPath) {
        return new TheLastResponseOfFirmaPagareField(xmlPath);
    }

    public static TheLastResponseOfFirmaPagareTimeResponse responseTime(TimeUnit responseTime) {
        return new TheLastResponseOfFirmaPagareTimeResponse(responseTime);
    }

	public static Question<String> tiempo() {
		return new PostQuestionBuilder<String>().to("https://10.169.104.203:7843/intf/ClientesHuellas/Enlace/V1.0")
				.with(request -> request.contentType(ContentType.XML)
						.body("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
								+ "  <soapenv:Header/>\n" + "  <soapenv:Body>\n"
								+ "    <cli:ClientesHuellas xmlns:cli=\"http://tuya.com/intf/ClientesHuellas\">\n"
								+ "      <tipoIdentificacion>1</tipoIdentificacion>\n"
								+ "      <numeroIdentificacion>1</numeroIdentificacion>\n"
								+ "    </cli:ClientesHuellas>\n" + "  </soapenv:Body>\n" + "</soapenv:Envelope>")
						.log().all().relaxedHTTPSValidation())
				.returning(response -> response.prettyPrint());


	}

}
