package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import co.com.tuya.certification.desmaterializacion.pagare.model.ConsultarBeneficiariosRequest;
import co.com.tuya.certification.desmaterializacion.pagare.model.dto.ObtenerInfoLogRequest;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerInforLog.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ObtenerInfoLogSolicitudDefinitions {
    @Cuando("^Yo ejecuto servicio ObtenerInformacionLog con lo siguientes datos numeroDocumento: \"([^\"]*)\" , numeroSolicitud: \"([^\"]*)\" y tipoDocumento: \"([^\"]*)\"$")
    public void yoEjecutoServicioObtenerInformacionLogConLoSiguientesDatosNumeroDocumentoNumeroSolicitudYTipoDocumento(String numeroId, String numeroSolicitud, String tipoId) {
        theActorInTheSpotlight().attemptsTo(Inquiry.promissoryNote(ObtenerInfoLogRequest.conNumeroId(numeroId).yNumeroSolicitud(numeroSolicitud).yTipoIdentificacion(tipoId).buildRequestForActor(theActorInTheSpotlight())));
    }


    @Entonces("^Valido respuesta del servicio ObtenerInformacionLog con codigoRespuesta: \"([^\"]*)\", mensajeRespuesta: \"([^\"]*)\", codigoDocumentoDesmaterializado: \"([^\"]*)\", nombreDocumentoDesmaterializado: \"([^\"]*)\", numeroDocumentoDesmaterializado: \"([^\"]*)\" , idDocumento:\"([^\"]*)\", codigoRespuestaFirma \"([^\"]*)\", descripcionRespuestaFirma: \"([^\"]*)\"$")
    public void validoRespuestaDelServicioObtenerInformacionLogConCodigoRespuestaMensajeRespuestaCodigoDocumentoDesmaterializadoNombreDocumentoDesmaterializadoNumeroDocumentoDesmaterializadoIdDocumentoCodigoRespuestaFirmaDescripcionRespuestaFirma(String codigoRespuesta, String mensajeRespuesta, String codigoDocumentoDesmaterializado, String nombreDocumentoDesmaterializado, String numeroDocumentoDesmaterializado, String idDocumento, String codigoRespuestaFirma, String descripcionRespuestaFirma) {
        if (codigoRespuesta.equals("OK00")){
            theActorInTheSpotlight().attemptsTo(ValidateTheLastResponse.ofFields(CAMPO_CODIGO_DOCUMENTO,CAMPO_NOMBRE_DOCUMENTO,CAMPO_CODIGO_RESPUESTA_FIRMA,CAMPO_DESCRIPCION_RESPUESTA_FIRMA).andTheRootPathIs(ROOT_PATH_XML_CONSULTA_INFO_LOG+".documentos.documento["+idDocumento+"]").hasTheItems(codigoDocumentoDesmaterializado,nombreDocumentoDesmaterializado,numeroDocumentoDesmaterializado,codigoRespuestaFirma,descripcionRespuestaFirma));
        }
        theActorInTheSpotlight().attemptsTo(ValidateTheLastResponse.ofFields(CAMPO_CODIGO_RESPUESTA,CAMPO_DESCRIPCION_RESPUESTA).andTheRootPathIs(ROOT_PATH_XML_CONSULTA_INFO_LOG).hasTheItems(codigoRespuesta,mensajeRespuesta));
    }

}
