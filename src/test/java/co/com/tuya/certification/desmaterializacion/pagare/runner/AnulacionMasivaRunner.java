package co.com.tuya.certification.desmaterializacion.pagare.runner;

import co.com.tuya.certification.desmaterializacion.pagare.utils.TY_ChangeLogo;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src//test//resources//features//AnulacionMasiva.feature",
        glue = {"co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions",
                "co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookAnulacionMasiva"},
        snippets = SnippetType.CAMELCASE, tags = {""}
)
public class AnulacionMasivaRunner {
    static TY_ChangeLogo logoSerenityReport;
    @AfterClass
    public static void finishTestExe() {
        logoSerenityReport = new TY_ChangeLogo();
        logoSerenityReport.changeLogo();
    }
}