package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.Post;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.AnulacionMasiva.*;
import static net.serenitybdd.rest.SerenityRest.rest;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.rest.abilities.CallAnApi.as;

public class AnulacionMasiva implements Task {

    private String idPagare;
    private String idSolicitud;

    public AnulacionMasiva(String idPagare,String idSolicitud){
        this.idPagare=idPagare;
        this.idSolicitud=idSolicitud;
    }
    public static AnulacionMasiva conCampos(String idPagare,String idSolicitud){
        return instrumented(AnulacionMasiva.class,idPagare,idSolicitud);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        //rest().get(as(actor).resolve(ORQUESTADOR));
        System.out.println("entre");
        actor.attemptsTo(Get.resource(ORQUESTADOR).with(requestSpecification -> requestSpecification
                .relaxedHTTPSValidation().param("idSolicitud",this.idSolicitud)
                .param("idPagare", this.idPagare)));
        System.out.println("sali");
    }
}
