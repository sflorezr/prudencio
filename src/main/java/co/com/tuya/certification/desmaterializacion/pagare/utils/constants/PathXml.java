package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.managers.PropertiesManager.XmlPath;

public class PathXml {
    public static final String XML_GENERAR_PAGARE= XmlPath.getString("xmlPathGenerarPagare");
    public static final String XML_OBTENER_PAGARE= XmlPath.getString("xmlPathObtenerPagare");
    public static final String XML_OBTENER_PDF_PAGARE= XmlPath.getString("xmlPathObtenerPdfPagare");
    public static final String XML_VALIDAR_CLIENTE_REGISTRADURIA= XmlPath.getString("xmlPathValidarClienteRegistraduria");
    public static final String XML_VALIDACION_HUELLA= XmlPath.getString("xmlPathValidacionPorHuella");
    public static final String XML_GENERAR_FIRMA_SOLICITUD= XmlPath.getString("xmlPathGenerarFirma");
    public static final String XML_CONSULTAR_BENEFICIARIOS= XmlPath.getString("xmlPathConsultaBeneficiarios");
    public static final String XML_CONSULTAR_INFOLOG= XmlPath.getString("xmlPathObtenerInfoLog");
}
