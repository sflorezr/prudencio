package co.com.tuya.certification.desmaterializacion.pagare.questions;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.CrearUnaConexionDeBaseDeDatos;
import co.com.tuya.certification.desmaterializacion.pagare.model.desmaterializacion.dao.LogDesmaterializacionSolicitudDao;
import co.com.tuya.certification.desmaterializacion.pagare.model.desmaterializacion.dto.LogDesmaterializacionSolicitud;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class MiraLogDesmaterializadosSolicitud implements Question<LogDesmaterializacionSolicitud>{
	private final String  numeroDocumento;
	private final String  numeroSolicitud;
	
	
	public MiraLogDesmaterializadosSolicitud(String numeroDocumento, String numeroSolicitud) {
		this.numeroDocumento = numeroDocumento;
		this.numeroSolicitud = numeroSolicitud;
	}

	@Override
	public LogDesmaterializacionSolicitud answeredBy(Actor actor) {		
		return CrearUnaConexionDeBaseDeDatos.como(actor).conectar().withExtension(LogDesmaterializacionSolicitudDao.class, LogDesmaterializacionSolicitudDao -> LogDesmaterializacionSolicitudDao.obtenerLogDesmaterializadoSolicitud(numeroDocumento, numeroSolicitud));
	}
	
	public static Question<LogDesmaterializacionSolicitud> logGenerado(String numeroDocumento, String numeroSolicitud){
		return new MiraLogDesmaterializadosSolicitud(numeroDocumento,numeroSolicitud);
	}

}
