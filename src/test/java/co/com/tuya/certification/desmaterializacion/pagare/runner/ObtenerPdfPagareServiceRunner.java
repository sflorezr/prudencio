package co.com.tuya.certification.desmaterializacion.pagare.runner;

import static cucumber.api.SnippetType.CAMELCASE;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import co.com.tuya.certification.desmaterializacion.pagare.utils.TY_ChangeLogo;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@CucumberOptions(features = "src//test//resources//features//obtener_pdf_pagare_service.feature",tags= {"@Ejecutar"} ,glue = {
		"co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions",
		"co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookobtenerpdfpagare" }, snippets = CAMELCASE

)
@RunWith(CucumberWithSerenity.class)
public class ObtenerPdfPagareServiceRunner {

	static TY_ChangeLogo logoSerenityReport;
	@AfterClass
	public static void finishTestExe() {
		logoSerenityReport = new TY_ChangeLogo();
		logoSerenityReport.changeLogo();
	}
}
