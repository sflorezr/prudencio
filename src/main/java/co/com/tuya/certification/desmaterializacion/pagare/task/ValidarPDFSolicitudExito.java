package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.LeerPdf;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

public class ValidarPDFSolicitudExito implements Task {
	private final String rutaPdf;
	
	public ValidarPDFSolicitudExito(String rutaPdf) {
		this.rutaPdf = rutaPdf;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
			LeerPdf.deLaRuta(rutaPdf);
	}

}
