package co.com.tuya.certification.desmaterializacion.pagare.questions;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.serenitybdd.screenplay.rest.questions.RestQueryFunction;

import java.util.List;
import java.util.function.Function;

public class PostQuestion<T> implements Question<T> {
	private final List<RestQueryFunction> queries;
	private final String endpoint;
	private final String name;
	private final Function<Response, T> result;

	public PostQuestion(String name, String endpoint, List<RestQueryFunction> queries, Function<Response, T> result) {
		this.name = name;
		this.endpoint = endpoint;
		this.queries = queries;
		this.result = result;
	}

	@Override
	public T answeredBy(Actor actor) {
		actor.attemptsTo(Post.to(endpoint).with(queries));
		return result.apply(SerenityRest.lastResponse());
	}

	@Override
	public String toString() {
		return name;
	}
}
