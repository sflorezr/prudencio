package co.com.tuya.certification.desmaterializacion.pagare.task;

import io.restassured.http.ContentType;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.GenerarPagareService.JUST_END_POINT;

public class Sign implements Task {
	private String bodyRequest;

	public Sign(String bodyRequest) {
		this.bodyRequest = bodyRequest;
	}

	public static Sign thePromissoryNote(String bodyRequest) {
		return Instrumented.instanceOf(Sign.class).withProperties(bodyRequest);
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Post.to(JUST_END_POINT).with(request -> request.contentType(ContentType.XML).body(bodyRequest)
                .relaxedHTTPSValidation()));

	}
}
