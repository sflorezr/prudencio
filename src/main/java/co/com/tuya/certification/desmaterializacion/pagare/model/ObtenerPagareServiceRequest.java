package co.com.tuya.certification.desmaterializacion.pagare.model;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ObtenerPagareServiceRequest {
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String numSolicitud;

    public ObtenerPagareServiceRequest(String numSolicitud) {

        this.numSolicitud = numSolicitud;
    }

    public static ObtenerPagareServiceRequest withNumeroSolicitud(String numeroSolicitud){
        return new ObtenerPagareServiceRequest(numeroSolicitud);
    }

    public ObtenerPagareServiceRequest andTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
        return this;
    }

    public ObtenerPagareServiceRequest andNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
        return this;
    }
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valorTipoIdentificacion",tipoIdentificacion).replace("valorNumeroIdentificacion",numeroIdentificacion).replace("valorNumeroSolicitud",numSolicitud);
    }


}
