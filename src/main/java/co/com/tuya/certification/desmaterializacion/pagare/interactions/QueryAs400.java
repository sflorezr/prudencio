package co.com.tuya.certification.desmaterializacion.pagare.interactions;

import co.com.tuya.certification.desmaterializacion.pagare.model.dto.QueryAtt0117;
import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class QueryAs400 {

    private static Logger LOGGER = Logger.getLogger(QueryAs400.class.getName());
    private QueryAtt0117 queryAtt0117;
    private List<Object> objectList;
    public QueryAs400(Object... objects) {


		this.queryAtt0117 = queryAtt0117;
	}

    protected ResultSet prepareStatement(Connection connection, String query){
        try{

            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for(int i=0;i<=objectList.size();i++){
                preparedStatement.setObject(i++,objectList.get(i));
            }
            preparedStatement.setInt(1,queryAtt0117.getNumeroDeSolicitud());
            preparedStatement.setString(2,queryAtt0117.getIdentificadorTrama());
            preparedStatement.setString(3,queryAtt0117.getNombreDelServicio());
            preparedStatement.setString(4,queryAtt0117.getCodigoDeRespuesta());
            ResultSet resultSet = preparedStatement.executeQuery();
            LOGGER.info("consulta a la base de datos exitosa");
            return resultSet;
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new BackendException("no se pudo consultar");
        }
    }

}
