Feature: Leer PDF

  @Exito
  Scenario: Caso de prueba Exito plantilla
    Given Visualizo PDF "NroSolicitudPR20190523143944", "exito"
    Then Valido campos
      | Cucuta                        |
      | 21/05/2019                    |
      | NroSolicitudPR                |
      | LineaFinanciamiento           |
      | Exito Poblado                 |
      |                            10 |
      | CodigoAsesor                  |
      |                       6000000 |
      | CuotaManejo                   |
      |                            25 |
      |                            17 |
      | Catt                          |
      | DireccionCatt                 |
      |                               |
      |                            20 |
      | PrimerApellido                |
      | CC M 0                        |
      |                    1020326598 |
      | 08/11/1991                    |
      |                             1 |
      | 23/11/2009                    |
      | LugarNacimiento               |
      | Casado                        |
      | Arrendada                     |
      |                           125 |
      | EstudioRealizado              |
      | DireccionResidencia           |
      |                               |
      |                               |
      | DepartamentoResidencia        |
      | LugarCorrespondencia          |
      |                               |
      | LugarCorrespondencia          |
      | NombreEmpresaLaboral          |
      | CargoActividad                |
      | DireccionLaboral              |
      | TipoActividad                 |
      | NO                            |
      | TipoOperacion                 |
      | ManejaRecursosPublicos_Cuales |
      |                      20000000 |
      |                            20 |
      | TipoProducto_OtroCual         |
      |                        200000 |
      | EntidadCuentaBancaria         |
      | RefFamNombre                  |
      | RefFamCiudad                  |

  @Carulla
  Scenario: Caso de prueba Carulla plantilla
    Given Visualizo PDF "NroSolicitudPR20190523150531", "carulla"
    Then Valido campos
      | Cucuta                        |
      | 21/05/2019                    |
      | NroSolicitudPR                |
      | LineaFinanciamiento           |
      | Exito Poblado                 |
      |                            10 |
      | CodigoAsesor                  |
      |                       6000000 |
      | CuotaManejo                   |
      |                            25 |
      |                            17 |
      | Catt                          |
      | DireccionCatt                 |
      |                               |
      |                            20 |
      | PrimerApellido                |
      | CC M 0                        |
      |                    1020326598 |
      | 08/11/1991                    |
      |                             1 |
      | 23/11/2009                    |
      | LugarNacimiento               |
      | Casado                        |
      | Arrendada                     |
      |                           125 |
      | EstudioRealizado              |
      | DireccionResidencia           |
      |                               |
      |                               |
      | DepartamentoResidencia        |
      | LugarCorrespondencia          |
      |                               |
      | LugarCorrespondencia          |
      | NombreEmpresaLaboral          |
      | CargoActividad                |
      | DireccionLaboral              |
      | TipoActividad                 |
      | NO                            |
      | TipoOperacion                 |
      | ManejaRecursosPublicos_Cuales |
      |                      20000000 |
      |                            20 |
      | TipoProducto_OtroCual         |
      |                        200000 |
      | EntidadCuentaBancaria         |
      | RefFamNombre                  |
      | RefFamCiudad                  |
