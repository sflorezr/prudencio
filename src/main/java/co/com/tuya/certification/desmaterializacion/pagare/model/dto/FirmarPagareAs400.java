package co.com.tuya.certification.desmaterializacion.pagare.model.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.StringJoiner;

public class FirmarPagareAs400 {

    private String numeroDeSolicitud;
    private String tipoDocumento;
    private String numeroDeDocumento;
    private String usuarioAsesorTuya;
    private String cattOrigen;
    private String codigoDeRespuesta;
    private String descripcionRespuesta;
    private String idPagareDeceval;


    public FirmarPagareAs400(String numeroDeSolicitud) {
        this.numeroDeSolicitud = numeroDeSolicitud;
    }


    public static FirmarPagareAs400 withNumeroDeSolicitud(String numeroDeSolicitud) {
        return new FirmarPagareAs400(numeroDeSolicitud);
    }


    public FirmarPagareAs400 andTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
        return this;
    }

    public FirmarPagareAs400 andNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
        return this;
    }

    public FirmarPagareAs400 andUsuarioAsesorTuya(String usuarioAsesorTuya) {
        this.usuarioAsesorTuya = usuarioAsesorTuya;
        return this;
    }

    public FirmarPagareAs400 andCattOrigen(String cattOrigen) {
        this.cattOrigen = cattOrigen;
        return this;
    }

    public FirmarPagareAs400 andCodigoDeRespuesta(String codigoDeRespuesta) {
        this.codigoDeRespuesta = codigoDeRespuesta;
        return this;
    }

    public FirmarPagareAs400 andIdPagareDeceval(String idPagareDeceval) {
        this.idPagareDeceval = idPagareDeceval;
        return this;
    }


    public FirmarPagareAs400 andDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
        return this;
    }


    public String getUsuarioAsesorTuya() {
        return usuarioAsesorTuya;
    }

    public String getCattOrigen() {
        return cattOrigen;
    }

    public String getCodigoDeRespuesta() {
        return codigoDeRespuesta;
    }


    public String getIdPagareDeceval() {
        return idPagareDeceval;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FirmarPagareAs400{");
        sb.append("numeroDeSolicitud='").append(numeroDeSolicitud).append('\'');
        sb.append(", tipoDocumento='").append(tipoDocumento).append('\'');
        sb.append(", numeroDeDocumento='").append(numeroDeDocumento).append('\'');
        sb.append(", usuarioAsesorTuya='").append(usuarioAsesorTuya).append('\'');
        sb.append(", cattOrigen='").append(cattOrigen).append('\'');
        sb.append(", codigoDeRespuesta='").append(codigoDeRespuesta).append('\'');
        sb.append(", descripcionRespuesta='").append(descripcionRespuesta).append('\'');
        sb.append(", idPagareDeceval='").append(idPagareDeceval).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
