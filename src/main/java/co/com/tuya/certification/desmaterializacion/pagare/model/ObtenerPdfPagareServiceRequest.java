package co.com.tuya.certification.desmaterializacion.pagare.model;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ObtenerPdfPagareServiceRequest {
    private String numSolicitud;

    public ObtenerPdfPagareServiceRequest(String numSolicitud) {
        this.numSolicitud = numSolicitud;
    }

    public static ObtenerPdfPagareServiceRequest withNumeroSolicitud(String numeroSolicitud){
        return new ObtenerPdfPagareServiceRequest(numeroSolicitud);
    }

    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valorNumeroSolicitud",numSolicitud);
    }


}
