package co.com.tuya.certification.desmaterializacion.pagare.model;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class GenerarFirmaSolicitudRequest {
	private String aplicativo;
	private String usuario;
	private String tipoDocumento;
	private String numeroDocumento;
	private String numeroSolicitud;
	
	public GenerarFirmaSolicitudRequest(String numeroDocumento) {
		this.numeroDocumento=numeroDocumento;
	}
	
	public static GenerarFirmaSolicitudRequest conNumeroDocumento(String NumeroDocumento) {
		return new GenerarFirmaSolicitudRequest(NumeroDocumento); 
	}
	
	public GenerarFirmaSolicitudRequest yTipoIdentificacion(String tipoIdentificacion) {
		this.tipoDocumento=tipoIdentificacion;
		return this;
	}
	
	public GenerarFirmaSolicitudRequest yAplicativo(String aplicativo) {
		this.aplicativo=aplicativo;
		return this;
	}
	
	public GenerarFirmaSolicitudRequest yUsuario(String usuario) {
		this.usuario=usuario;
		return this;
	}
	public GenerarFirmaSolicitudRequest yNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud=numeroSolicitud;
		return this;
	}
	
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valorNumeroSolicitud",numeroSolicitud).replace("valorAplicativo", aplicativo).replace("valorUsuario", usuario).replace("valorTipoDocumento", tipoDocumento).replace("valorNumeroDocumento", numeroDocumento);
    }
	
}
