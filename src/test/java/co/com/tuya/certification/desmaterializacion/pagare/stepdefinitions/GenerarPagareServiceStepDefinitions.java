package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import co.com.tuya.certification.desmaterializacion.pagare.task.Sign;
import co.com.tuya.certification.desmaterializacion.pagare.task.Validate;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static co.com.tuya.certification.desmaterializacion.pagare.model.builder.GenerarPagareServiceBuilder.withNumeroSolicitud;
import static co.com.tuya.certification.desmaterializacion.pagare.model.dto.GenerarPagareResponse.withCodigoRespuesta;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class GenerarPagareServiceStepDefinitions {

    @When("^I sign the promissory note with numeroSolicitud: \"([^\"]*)\" with my clave:\"([^\"]*)\" with my document with \"([^\"]*)\" and numeroDocumento:\"([^\"]*)\" with my fingerprint:\"([^\"]*)\"$")
    public void iSignThePromissoryNoteWithNumeroSolicitudWithMyClaveWithMyDocumentWithAndNumeroDocumentoWithMyFingerprint(String numeroSolicitud, String clave, String tipoDocumento, String numeroDocumento, String contenido) {
        theActorInTheSpotlight().attemptsTo(
                Sign.thePromissoryNote(withNumeroSolicitud(numeroSolicitud).andClave(clave).andTipoDocumento(tipoDocumento).andNumeroDocumento(numeroDocumento).andContenido(contenido).buildForActor(theActorInTheSpotlight())));
    }

    @Then("^I should see the responseCode:\"([^\"]*)\" with responseMessage:\"([^\"]*)\"$")
    public void iShouldSeeTheResponseCodeWithResponseMessage(String codigoRespuesta, String mensajeRespuesta) {
        theActorInTheSpotlight().attemptsTo(
                Validate.theLastResponseOfFirmaPagare(withCodigoRespuesta(codigoRespuesta).andMensajeRespuesta(mensajeRespuesta)));
    }
}
