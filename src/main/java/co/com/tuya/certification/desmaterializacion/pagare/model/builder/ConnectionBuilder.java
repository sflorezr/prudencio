package co.com.tuya.certification.desmaterializacion.pagare.model.builder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;

public class ConnectionBuilder {
	private static final Logger LOGGER = Logger.getLogger(ConnectionBuilder.class.getName());

	private String user;
	private String password;
	private String url;

	public ConnectionBuilder(String url) {
		this.url = url;

	}

	public static ConnectionBuilder createNewConnectionTo(String url) {
		return new ConnectionBuilder(url);
	}

	public ConnectionBuilder withUser(String user) {
		this.user = user;
		return this;
	}

	public ConnectionBuilder andPassword(String password) {
		this.password = password;
		return this;
	}

	public Connection build() {
		try {
			Connection connection = DriverManager.getConnection(url, user, password);
			LOGGER.info("conexion exitosa a la base de datos");
			return connection;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
			throw new BackendException(
					"no se pudo realizar la conexion a " + url + " con usuario:" + user + " y password:" + password);
		}

	}
}
