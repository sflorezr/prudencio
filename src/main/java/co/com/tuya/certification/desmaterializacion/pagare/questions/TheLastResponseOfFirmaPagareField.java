package co.com.tuya.certification.desmaterializacion.pagare.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.annotations.Subject;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
@Subject("the field with xmlPath #xmlPath")
public class TheLastResponseOfFirmaPagareField implements Question<String> {
	private String xmlPath;
    public TheLastResponseOfFirmaPagareField(String xmlPath) {
        this.xmlPath = xmlPath;
    }
    @Override
    public String answeredBy(Actor actor) {
      
        return LastResponse.received().answeredBy(actor).xmlPath().setRoot("Envelope.Body.generarPagareResponse").getString(xmlPath);
    }

}
