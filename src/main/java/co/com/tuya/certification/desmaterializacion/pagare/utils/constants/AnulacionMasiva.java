package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class AnulacionMasiva {
    public static final String HOST ="10.169.104.206";
    public static final int PUERTO = 7083;
    public static final String ESQUEMA = "https";
    public  static  final String ORQUESTADOR = "/s009-v1.0/vinculaciones";
}
