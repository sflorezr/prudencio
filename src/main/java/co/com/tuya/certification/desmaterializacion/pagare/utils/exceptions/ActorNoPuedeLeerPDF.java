package co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions;

public class ActorNoPuedeLeerPDF extends RuntimeException{
	public ActorNoPuedeLeerPDF(String Actor) {
		super("El actor "+Actor+" no tiene la abilidad para leer PDF");
	}
}
