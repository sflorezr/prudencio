package co.com.tuya.certification.desmaterializacion.pagare.interactions;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.LeerPDF;
import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import co.com.tuya.certification.desmaterializacion.pagare.utils.managers.PropertiesManager;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;

public class LeerPdf implements Interaction {
	private final String rutaPdf;
	private String[] PDFlista;
	private String plantilla;
	private List<String> valoresPdf=new ArrayList<String>();
	private static final Logger LOGGER = Logger.getLogger(LeerPDF.class.getName());
	public LeerPdf(String rutaPdf) {
		this.rutaPdf = rutaPdf;
	}
	
	public LeerPdf conPlantilla(String plantilla) {
		this.plantilla=plantilla;
		return this;	
	}
	@Override
	public <T extends Actor> void performAs(T actor) {
		String[] lista=null;
		String linea="";
		String lineaTemp="";
		Path pdf=Paths.get(rutaPdf);
		try {
			Serenity.recordReportData().asEvidence().withTitle("PDF").downloadable().fromFile(pdf);
			PDFlista=LeerPDF.as(actor).resolve();
			switch (plantilla) {
			case "exito":
				lista=PropertiesManager.ListaPDF.getString("lista").split(",");	
				break;
			case "carulla":
				lista=PropertiesManager.ListaPDFCarulla.getString("lista").split(",");	
				break;
			default:
				break;
			}
		
			
			for(String lineas:lista) {
				lineaTemp="";
				switch (plantilla) {
				case "exito":
					linea=PropertiesManager.ListaPDF.getString(lineas.split(",")[0]);	
					break;
				case "carulla":
					linea=PropertiesManager.ListaPDFCarulla.getString(lineas.split(",")[0]);	
					break;
				default:
					break;
				}				
				if(linea.split(",")[1].split("-").length>1) {
				
					for (int i = Integer.parseInt((linea.split(",")[1].split("-")[0])); i < Integer.parseInt((linea.split(",")[1].split("-")[1])); i++) {
						if (lineaTemp.equals("")) {
							if (PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ").length>i) {
								lineaTemp=PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ")[i];	
							}							
						}else {
							if (PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ").length>i) {
								lineaTemp=lineaTemp+" "+PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ")[i];	
							}													
						}
					}
				}else {
					if (PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ").length>Integer.parseInt(linea.split(",")[1])) {
						lineaTemp=PDFlista[Integer.parseInt(linea.split(",")[0])].split(" ")[Integer.parseInt(linea.split(",")[1])];	
					}else {
						if (linea.split(",")[1].equals("99")) {
							lineaTemp=PDFlista[Integer.parseInt(linea.split(",")[0])];
						}
					}
				}
				
				lineaTemp=lineaTemp.replace(",", "");
				lineaTemp=lineaTemp.replace("$", "");
				lineaTemp=lineaTemp.replace(".", "");
				valoresPdf.add(lineaTemp);
							
			}
			actor.remember("PDFleido", valoresPdf);
		} catch (IOException e) {
			LOGGER.error(e.getCause());
			throw new BackendException("hubo problemas con la lectura del PDF");
		}
		
	}
	public static LeerPdf deLaRuta(String rutaPdf) {
		return Instrumented.instanceOf(LeerPdf.class).withProperties(rutaPdf);
	}

}
