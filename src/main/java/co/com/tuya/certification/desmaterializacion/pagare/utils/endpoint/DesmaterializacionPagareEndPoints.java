package co.com.tuya.certification.desmaterializacion.pagare.utils.endpoint;

public class DesmaterializacionPagareEndPoints {
    public static String END_POINT_GENERAR_PAGARE="https://brokerlab.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0";
    public static String END_POINT_OBTENER_PAGARE="https://brokerlab.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0";
    public static String END_POINT_ANULACION_MASIVA="https://10.169.104.206:7083";
    public static String USUARIO="USUARIO";

}
