#language:es
Característica: ValidarClienteRegistraduria

  @Ejecutar
  Esquema del escenario: Consulto y valido escenario con estado de validacion VLM
    Cuando Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:"<tipoIdentificacion>" y numeroIdentificacion:"<numeroIdentificacion>" y excedeTiempo:"<excedeTiempo>" y estadoValidacion:"<estadoValidacion>" y fechaActual:"<fechaActual>"
    Entonces Valido respuesta del SOAP con codigoRespuesta:"<codigoRespuesta>" y mensajeRespuesta:"<mensajeRespuesta>" y observacion:"<observacion>" with capturaHuella:"<capturaHuella>" y estadoValidacion:"<estadoValidacion>"
    Cuando Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: "<tipoIdentificacion>" y numeroIdentificacion: "<numeroIdentificacion>"
    Entonces valido respuesta del archivo con estadoValidacion:"<estadoValidacion>"

    Ejemplos:
      | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta | observacion                                                                                          | estadoValidacion | capturaHuella | excedeTiempo | fechaActual |
      | 1                  | 1036621742           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | VLM              | N             | N            | S           |
      | 1                  | 1022325895           | 00              | Consulta Exitosa | Debe realizar validaciones manuales                                                                  | VLM              | N             | S            | S           |
      | 1                  | 1036621749           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | VLM              | N             | N            | N           |

  @Ejecutar1
  Esquema del escenario: Consulto y valido escenario con estado de validacion VCX
    Cuando Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:"<tipoIdentificacion>" y numeroIdentificacion:"<numeroIdentificacion>" y excedeTiempo:"<excedeTiempo>" y estadoValidacion:"<estadoValidacion>" y fechaActual:"<fechaActual>"
    Entonces Valido respuesta del SOAP con codigoRespuesta:"<codigoRespuesta>" y mensajeRespuesta:"<mensajeRespuesta>" y observacion:"<observacion>" with capturaHuella:"<capturaHuella>" y estadoValidacion:"<estadoValidacion>"
    Cuando Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: "<tipoIdentificacion>" y numeroIdentificacion: "<numeroIdentificacion>"
    Entonces valido respuesta del archivo con estadoValidacion:"<estadoValidacion>"

    Ejemplos:
      | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta | observacion                                                                                          | estadoValidacion | capturaHuella | excedeTiempo | fechaActual |
      | 1                  | 1090442524           | 00              | Consulta Exitosa | Realizar Captura Huella                                                                              | VCX              | S             | S            | S           |
      | 1                  | 1000392841           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | VCX              | N             | N            | S           |
      | 1                  | 1000392849           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | VCX              | N             | N            | N           |

  @Ejecutar
  Esquema del escenario: Consulto y valido escenario con estado de validacion NAE
    Cuando Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:"<tipoIdentificacion>" y numeroIdentificacion:"<numeroIdentificacion>" y excedeTiempo:"<excedeTiempo>" y estadoValidacion:"<estadoValidacion>" y fechaActual:"<fechaActual>"
    Entonces Valido respuesta del SOAP con codigoRespuesta:"<codigoRespuesta>" y mensajeRespuesta:"<mensajeRespuesta>" y observacion:"<observacion>" with capturaHuella:"<capturaHuella>" y estadoValidacion:"<estadoValidacion>"
    Cuando Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: "<tipoIdentificacion>" y numeroIdentificacion: "<numeroIdentificacion>"
    Entonces valido respuesta del archivo con estadoValidacion:"<estadoValidacion>"

    Ejemplos:
      | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta | observacion                                                                                          | estadoValidacion | capturaHuella | excedeTiempo | fechaActual |
      | 1                  | 1017184060           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | NAE              | N             | N            | S           |
      | 1                  | 1017184061           | 00              | Consulta Exitosa | El Cliente no ha sido autorizado                                                                     | NAE              | N             | S            | S           |
      | 1                  | 1017184069           | 00              | Consulta Exitosa | Validar primero Cliente en Registraduria para continuar con la captura de huella en Motor Biometrico | NAE              | N             | N            | N           |

  @Ejecutar
  Esquema del escenario: Consulto y valido escenario con estado de validacion 0
    Cuando Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:"<tipoIdentificacion>" y numeroIdentificacion:"<numeroIdentificacion>" y excedeTiempo:"<excedeTiempo>" y estadoValidacion:"<estadoValidacion>" y fechaActual:"<fechaActual>"
    Entonces Valido respuesta del SOAP con codigoRespuesta:"<codigoRespuesta>" y mensajeRespuesta:"<mensajeRespuesta>" y observacion:"<observacion>" with capturaHuella:"<capturaHuella>" y estadoValidacion:"<estadoValidacion>"
    Cuando Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: "<tipoIdentificacion>" y numeroIdentificacion: "<numeroIdentificacion>"
    Entonces valido respuesta del archivo con estadoValidacion:"<estadoValidacion>"

    Ejemplos:
      | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta | observacion                                  | estadoValidacion | capturaHuella | excedeTiempo | fechaActual |
      | 1                  | 123456               | 00              | Consulta Exitosa | No se encuentra informacion para esta cedula | 0                | N             | N            | N           |
      | 2                  | 1090442524           | 00              | Consulta Exitosa | No se encuentra informacion para esta cedula | 0                | N             | N            | N           |

  @Ejecutar
  Esquema del escenario: Consulto y valido escenario con estado de validacion sin parametrizar
    Cuando Consulto el servicio ValidarClienteRegistraduria con tipoDocumento:"<tipoIdentificacion>" y numeroIdentificacion:"<numeroIdentificacion>" y excedeTiempo:"<excedeTiempo>" y estadoValidacion:"<estadoValidacion>" y fechaActual:"<fechaActual>"
    Entonces Valido respuesta del SOAP con codigoRespuesta:"<codigoRespuesta>" y mensajeRespuesta:"<mensajeRespuesta>" y observacion:"<observacion>" with capturaHuella:"<capturaHuella>" y estadoValidacion:"<estadoValidacion>"
    Cuando Consulto AS400 en el archivo ATT0041 con  and tipoIdentificacion: "<tipoIdentificacion>" y numeroIdentificacion: "<numeroIdentificacion>"
    Entonces valido respuesta del archivo con estadoValidacion:"<estadoValidacion>"

    Ejemplos:
      | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta | observacion                                     | estadoValidacion | capturaHuella | excedeTiempo | fechaActual |
      | 1                  | 55550                | 00              | Consulta Exitosa | La descripcion de la Resp no esta parametrizada | XX1              | N             | N            | N           |
      | 1                  | 55551                | 00              | Consulta Exitosa | La descripcion de la Resp no esta parametrizada | XX2              | N             | N            | S           |
      | 1                  | 55552                | 00              | Consulta Exitosa | La descripcion de la Resp no esta parametrizada | XX3              | N             | S            | N           |
      | 1                  | 55553                | 00              | Consulta Exitosa | La descripcion de la Resp no esta parametrizada | XX4              | N             | S            | S           |

  @Ejecutar1
  Escenario: Validar parametros SA0050
  A50NPR= PTT0095A

    Cuando Consulto en el archivo SA0050 con filtro A50NPR=PTT0095A
    Entonces Valido respuesta de parametros para respuestas del servicio SOAP
      | 1 | 10                                                 | Tiempo minimo para realizar la consulta(Minutos)   |
      | 2 | El Cliente no ha sido autorizado                   |                                                    |
      | 3 | Recuerde realizar validaciones manuales            |                                                    |
      | 4 | N                                                  | Campo para habilitar la captura de huella N        |
      | 5 | Realizar Captura Huella                            |                                                    |
      | 6 | Validar primero Cliente en Registraduria para cont | inuar con la captura de huella en Motor Biometrico |
      | 7 | Por favor realice el proceso de validación con RNE | C antes de continuar                               |
      | 8 | S                                                  | Campo para habilitar la captura de huella S        |