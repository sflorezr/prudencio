Feature: Obtener pdf pagare Inquiry if a customer has already
  As an User
  I want to Inquiry a prommisory note
  to see if the customer has already signed it

  Scenario Outline: Inquiry the promissory note
    When I inquiry the service with numeroSolicitud:"<numSolicitud>"
    Then I should see that the response the service ObtenerPdfPagare has codigoRespuesta:"<codigoRespuesta>" and mensajeRespuesta:"<mensajeRespuesta>" and estado:"<estadoPagare>" with fecha:"<fechaGrabacionPagare>" and idPagareDeceval:"<idPagareDeceval>" and nombreOtorgante: "<nombreOtorgante>" and numeroDocumentoOtorgante: "<numeroDocumentoOtorgante>" and tipoDocumento: "<tipoDocumento>"

    Examples: 
      | numSolicitud | codigoRespuesta | mensajeRespuesta                                                | estadoPagare           | fechaGrabacionPagare          | idPagareDeceval | nombreOtorgante             | numeroDocumentoOtorgante | tipoDocumento        |
      | 000700315562 |            0000 | CONSULTA EXITOSA                                                | Registrado - En Blanco | 2018-10-05T14:15:07.762-05:00 |          199688 | LINA BUENAHORA              |               1098709766 | CEDULA DE CIUDADANIA |
      | 000700482617 |            0000 | CONSULTA EXITOSA                                                | Registrado - En Blanco | 2019-03-06T07:12:39.443-05:00 |          284492 | LUISA MUNOZ                 |                 12138449 | CEDULA DE CIUDADANIA |
      | 000700484028 |            0000 | CONSULTA EXITOSA                                                | Registrado - En Blanco | 2019-03-12T16:52:02.030-05:00 |          286887 | CARLA MARIA PEREIRA HIGUITA |                 19432030 | CEDULA DE CIUDADANIA |
      | 000700482619 | OK01            | No existe información para los criterios ingresados             |                        |                               |                 |                             |                          |                      |
      |              | OK01            | No existe información para los criterios ingresados             |                        |                               |                 |                             |                          |                      |
      | 000700485313 | SDL.SE.0118     | SDL.SE.0118: No existe información con los criterios ingresados |                        |                               |                 |                             |                          |                      |

  @Ejecutar
  Scenario Outline: Inquiry the promissory note
    When I inquiry the service with numeroSolicitud:"<numSolicitud>"
 #   Then I should see that the response the service ObtenerPdfPagare has codigoRespuesta:"<codigoRespuesta>" and mensajeRespuesta:"<mensajeRespuesta>" and estado:"<estadoPagare>" with fecha:"<fechaGrabacionPagare>" and idPagareDeceval:"<idPagareDeceval>" and nombreOtorgante: "<nombreOtorgante>" and numeroDocumentoOtorgante: "<numeroDocumentoOtorgante>" and tipoDocumento: "<tipoDocumento>"

    Examples: 
      | numSolicitud | codigoRespuesta | mensajeRespuesta | estadoPagare           | fechaGrabacionPagare          | idPagareDeceval | nombreOtorgante | numeroDocumentoOtorgante | tipoDocumento        |
      |    394042817 |            0000 | CONSULTA EXITOSA | Registrado - En Blanco | 2018-10-05T14:15:07.762-05:00 |          199688 | LINA BUENAHORA  |               1098709766 | CEDULA DE CIUDADANIA |
         