package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.FIELD_CODIGO_RESPUESTA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.FIELD_ESTADO_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.FIELD_FECHA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.FIELD_HORA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.FIELD_MENSAJE_RESPUESTA_RESPONSE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ObtenerPagareService.ROOT_PATH_XML_OBTENER_PAGARE;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.tuya.certification.desmaterializacion.pagare.model.ObtenerPagareServiceRequest;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ObtenerPagareServiceStepDefinitions {
    @When("^I inquiry the numeroSolicitud:\"([^\"]*)\" of a customer with tipoIdentificacion:\"([^\"]*)\" and numeroIdentificacion:\"([^\"]*)\"$")
    public void iInquiryTheNumeroSolicitudOfACustomerWithTipoIdentificacionAndNumeroIdentificacion(String numeroSolicitud, String tipoIdentificacion, String numeroIdentificacion) {
        theActorInTheSpotlight().attemptsTo(
                Inquiry.promissoryNote(ObtenerPagareServiceRequest.withNumeroSolicitud(numeroSolicitud).andTipoIdentificacion(tipoIdentificacion).andNumeroIdentificacion(numeroIdentificacion).buildRequestForActor(theActorInTheSpotlight()))
        );
    }

    @Then("^I should see that the response has codigoRespuesta:\"([^\"]*)\" and mensajeRespuesta:\"([^\"]*)\" and estado:\"([^\"]*)\" with fecha:\"([^\"]*)\" and hora:\"([^\"]*)\"$")
    public void iShouldSeeThatTheResponseHasCodigoRespuestaAndMensajeRespuestaAndEstadoWithFechaAndHora(String codigoRespuesta, String mensajeRespuesta, String estado, String fecha, String hora) {
        theActorInTheSpotlight().attemptsTo(
                ValidateTheLastResponse.ofFields(FIELD_CODIGO_RESPUESTA_RESPONSE, FIELD_MENSAJE_RESPUESTA_RESPONSE, FIELD_ESTADO_RESPONSE, FIELD_FECHA_RESPONSE, FIELD_HORA_RESPONSE).andTheRootPathIs(ROOT_PATH_XML_OBTENER_PAGARE).hasTheItems(codigoRespuesta, mensajeRespuesta, estado, fecha, hora)
        );
    }

}

