package co.com.tuya.certification.desmaterializacion.pagare.task;

import co.com.tuya.certification.desmaterializacion.pagare.questions.TheValue;
import com.google.common.collect.ImmutableList;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.List;

import static java.util.Arrays.asList;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.*;

public class ValidateTheLastResponse implements Task {
    private List<String> fields;
    private List<String> items;
    private String rootPath;

    public ValidateTheLastResponse(List<String> fields) {
       this.fields=ImmutableList.copyOf(fields);
    }

    public ValidateTheLastResponse hasTheItems(String... items) {
        this.items = asList(items);
        return this;
    }

    public ValidateTheLastResponse andTheRootPathIs(String rootPath) {
        this.rootPath = rootPath;
        return this;
    }

    public static ValidateTheLastResponse ofFields(String... fields) {
        return Tasks.instrumented(ValidateTheLastResponse.class,asList(fields));
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        fields.forEach(field->actor.should(
                seeThat(TheValue.ofField(field).withRoothPath(rootPath),isIn(items))
        ));


    }
}
