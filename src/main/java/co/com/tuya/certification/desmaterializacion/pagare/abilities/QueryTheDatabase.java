package co.com.tuya.certification.desmaterializacion.pagare.abilities;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.ActorCannotQueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;
import org.apache.log4j.Logger;

public class QueryTheDatabase implements Ability {
	private static final Logger LOGGER = Logger.getLogger(QueryTheDatabase.class.getName());

	public QueryTheDatabase() {
		try {
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {
			LOGGER.error(e.getMessage());
			throw new BackendException(
					"com.ibm.as400.access.AS400JDBCDriver, debes cargar el jar jt400 en el proyecto");
		}
	}

	public static QueryTheDatabase as400() {
		return new QueryTheDatabase();
	}

	public static QueryTheDatabase as(Actor actor) {
		if (actor.abilityTo(QueryTheDatabase.class) == null) {
			throw new ActorCannotQueryTheDatabase(actor.getName());
		}
		return actor.abilityTo(QueryTheDatabase.class);
	}

	@Override
	public String toString() {
		return "Query the database";
	}
}
