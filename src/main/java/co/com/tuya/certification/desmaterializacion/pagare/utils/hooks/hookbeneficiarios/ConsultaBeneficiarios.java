package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookbeneficiarios;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.CrearUnaConexionDeBaseDeDatos;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.QueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.Before;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.XML_CONSULTAR_BENEFICIARIOS;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ConsultaBeneficiarios {

    private static final String USER_SERVICE = "USER SERVICES";

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
        theActorCalled(USER_SERVICE).whoCan(CallAnApi.at("https://10.169.4.41:10064/SegurosV1.svc"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_CONSULTAR_BENEFICIARIOS));
    }
}
