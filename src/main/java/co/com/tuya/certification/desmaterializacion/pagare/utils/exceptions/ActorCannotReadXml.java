package co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions;

public class ActorCannotReadXml extends RuntimeException {

	public ActorCannotReadXml(String actorName) {
		super("The actor " + actorName + " does not have the ability to read a xml file");
	}
}
