package co.com.tuya.certification.desmaterializacion.pagare.model.dto;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ObtenerInfoLogRequest {
    private String numeroId="";
    private String numeroSolicitud="";
    private String tipoIdentificacion="";

    public ObtenerInfoLogRequest(String numeroId) {
        this.numeroId = numeroId;
    }
    public static ObtenerInfoLogRequest conNumeroId(String numeroId){
        return new ObtenerInfoLogRequest(numeroId);
    }

    public ObtenerInfoLogRequest yTipoIdentificacion(String tipoIdentificacion){
        this.tipoIdentificacion=tipoIdentificacion;
        return this;
    }
    public ObtenerInfoLogRequest yNumeroSolicitud(String numeroSolicitud){
        this.numeroSolicitud=numeroSolicitud;
        return this;
    }
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valorNumeroDocumento",numeroId).replace("valorTipoDocumento",tipoIdentificacion).replace("valorNumeroSolicitud",numeroSolicitud);
    }
}
