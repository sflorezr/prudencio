package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookAnulacionMasiva;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.specification.ProxySpecification;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.AnulacionMasiva.*;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.endpoint.DesmaterializacionPagareEndPoints.*;

public class AnulacionMasiva {
    @Before
    public void prepararRest(){
        setTheStage(new OnlineCast());
        theActorCalled(USUARIO).whoCan(CallAnApi.at("https://10.169.104.206:7083"));
        SerenityRest.setDefaultProxy(new ProxySpecification(HOST,PUERTO,ESQUEMA));
    }

    @After
    public void LiberarAnulacion(){drawTheCurtain();}
}
