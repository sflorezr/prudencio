package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import co.com.tuya.certification.desmaterializacion.pagare.model.ConsultarBeneficiariosRequest;
import co.com.tuya.certification.desmaterializacion.pagare.task.Inquiry;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponse;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.ConsultaBeneficiarios.*;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ConsultarBeneficiariosStepDefinitions {

    @When("^Yo ejecuto servicio ConsultarBeneficiarios con lo siguientes datos numeroDocumento: \"([^\"]*)\" , numeroSolicitud: \"([^\"]*)\"$")
    public void yoEjecutoServicioConsultarBeneficiariosConLoSiguientesDatosNumeroDocumentoNumeroSolicitud(String numeroId, String numeroSolicitud) {
        theActorInTheSpotlight().attemptsTo(Inquiry.promissoryNote(ConsultarBeneficiariosRequest.conNumeroId(numeroId).yNumeroSolicitud(numeroSolicitud).buildRequestForActor(theActorInTheSpotlight())));
    }

    @Then("^Valido respuesta del servicio ConsultarBeneficiarios con codigoRespuesta: \"([^\"]*)\", mensajeRespuesta: \"([^\"]*)\", numeroBeneficiario: \"([^\"]*)\", apellidosBeneficiarios: \"([^\"]*)\", cedulaBeneficiario: \"([^\"]*)\"$")
    public void validoRespuestaDelServicioConsultarBeneficiariosConCodigoRespuestaMensajeRespuestaNumeroBeneficiarioApellidosBeneficiariosCedulaBeneficiario(String codigoRespuesta, String mensajeRespuesta, String indice, String apellido, String documento) {
        if(codigoRespuesta.equals("2000")){
            theActorInTheSpotlight().attemptsTo(ValidateTheLastResponse.ofFields(FIELD_APELLIDO,FIELD_DOCUMENTO).andTheRootPathIs(ROOT_PATH_XML_CONSULTA_BENEFICIARIOS+".Beneficiarios.Beneficiario["+indice+"]").hasTheItems(apellido,documento));
        }
        theActorInTheSpotlight().attemptsTo(ValidateTheLastResponse.ofFields(FIELD_RESPUESTA,FIELD_CODIGO).andTheRootPathIs(ROOT_PATH_XML_CONSULTA_BENEFICIARIOS).hasTheItems(codigoRespuesta,mensajeRespuesta));
    }

}
