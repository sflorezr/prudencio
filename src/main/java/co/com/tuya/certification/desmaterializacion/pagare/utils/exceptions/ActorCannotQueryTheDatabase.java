package co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions;

public class ActorCannotQueryTheDatabase extends RuntimeException {
	public ActorCannotQueryTheDatabase(String actorName) {
		super("The actor " + actorName + " does not have the ability to QueryInteraction the Database");
	}
}
