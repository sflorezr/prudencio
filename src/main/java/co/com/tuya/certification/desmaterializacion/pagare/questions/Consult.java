package co.com.tuya.certification.desmaterializacion.pagare.questions;

import static java.util.Arrays.asList;

import java.sql.*;
import java.util.List;

import net.serenitybdd.screenplay.Interaction;
import net.thucydides.core.annotations.Step;
import org.apache.log4j.Logger;

import co.com.tuya.certification.desmaterializacion.pagare.utils.TomaEvidencia;
import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.BackendException;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;

public class Consult implements Interaction {
    private static final Logger LOGGER = Logger.getLogger(Consult.class.getName());
    private String query;
    private String Cadena="";
    private List<Object> parameters;
    private static ResultSet lastResponseAs400;
    private static ResultSet Evidencia;
    private static ResultSetMetaData prueba;
    public Consult(String query) {
        this.query = query;
    }

    public Consult withParameters(Object... parameters) {
        this.parameters = asList(parameters);
        return this;
    }
    @Step("{1} Selecting from AS400")
    @Override
    public <T extends Actor> void performAs(T actor) {
        final Connection connection = actor.recall("ConexionAs400");
        int indexPrepareStatementr = 1;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (Object parameter : parameters) {
                preparedStatement.setObject(indexPrepareStatementr, parameter);
                indexPrepareStatementr++;
            }
            lastResponseAs400 = preparedStatement.executeQuery();

            TomaEvidencia.deBaseDeDatos(lastResponseAs400);
            indexPrepareStatementr = 1;
            for (Object parameter : parameters) {
                preparedStatement.setObject(indexPrepareStatementr, parameter);
                indexPrepareStatementr++;
            }
            lastResponseAs400 = preparedStatement.executeQuery();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
            throw new BackendException("no se pudo preparar el query para as400");
        }


    }

    public static Consult logWithQuery(String query) {
        return new Consult(query);
    }

    public static ResultSet getLastResponse() {
        return lastResponseAs400;
    }



}
