package co.com.tuya.certification.desmaterializacion.pagare.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CodigoRespuesta implements Question {
    public static Question<Integer> fue() {return new CodigoRespuesta();}
    @Override
    public Object answeredBy(Actor actor){return SerenityRest.lastResponse().statusCode();
    }
}
