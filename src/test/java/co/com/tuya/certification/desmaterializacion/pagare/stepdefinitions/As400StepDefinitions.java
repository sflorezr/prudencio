package co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.PASSWORD_AS400;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_ATT0117;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.QUERY_ATT0119;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.URL_DATABASE;
import static co.com.tuya.certification.desmaterializacion.pagare.utils.queries.QueryForFile.USER_AS400;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.tuya.certification.desmaterializacion.pagare.interactions.CreateConnection;
import co.com.tuya.certification.desmaterializacion.pagare.questions.Consult;
import co.com.tuya.certification.desmaterializacion.pagare.task.ValidateTheLastResponseOfAs400;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class As400StepDefinitions {
    @When("^I inquiry the AS400 whith numeroSolicitud: \"([^\"]*)\" and tipoIdentificacion: \"([^\"]*)\" and numeroIdentificacion: \"([^\"]*)\" and codigoRespuesta: \"([^\"]*)\" and mensajeRespuesta: \"([^\"]*)\"$")
    public void iInquiryTheASWhithNumeroSolicitudAndTipoIdentificacionAndNumeroIdentificacionAndCodigoRespuestaAndMensajeRespuesta(String numeroSolicitud, String tipoIdentificacion, String numeroIdentificacion, String codigoRespuesta, String mensajeRespuesta) {
        theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                Consult.logWithQuery(QUERY_ATT0117).withParameters(numeroSolicitud, numeroIdentificacion, codigoRespuesta, mensajeRespuesta));
    }

    @When("^I should see that the response has numeroSolicitud: \"([^\"]*)\" and tipoIdentificacion: \"([^\"]*)\" and numeroIdentificacion: \"([^\"]*)\" and codigoRespuesta: \"([^\"]*)\" and mensajeRespuesta: \"([^\"]*)\"$")
    public void iShouldSeeThatTheResponseHasNumeroSolicitudAndTipoIdentificacionAndNumeroIdentificacionAndCodigoRespuestaAndMensajeRespuesta(String numeroSolicitud, String tipoIdentificacion, String numeroIdentificacion, String codigoRespuesta, String mensajeRespuesta) {
    	/*	if (!codigoRespuesta.equals("vacio") && !mensajeRespuesta.equals("vacio")) {
    		theActorInTheSpotlight().attemptsTo(ValidateTheLastResponseOfAs400.ofFields("A0117NUSOL", "A0117NUMID", "A0117CODRE", "A0117DESRE").hasTheItems(String.valueOf(Integer.parseInt(numeroSolicitud)), numeroIdentificacion, codigoRespuesta, mensajeRespuesta));	
    	}  */  	    		
    }
    @When("^I inquiry the AS400 in file ATT0117 whith numeroSolicitud: \"([^\"]*)\" and tipoIdentificacion: \"([^\"]*)\" and numeroIdentificacion: \"([^\"]*)\" and codigoRespuesta: \"([^\"]*)\" and mensajeRespuesta: \"([^\"]*)\"$")
    public void iInquiryTheASInFileATT0117WhithNumeroSolicitudAndTipoIdentificacionAndNumeroIdentificacionAndCodigoRespuestaAndMensajeRespuesta(String numeroSolicitud, String tipoIdentificacion, String numeroIdentificacion, String codigoRespuesta, String mensajeRespuesta) {
    	 theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                 Consult.logWithQuery(QUERY_ATT0117).withParameters(numeroSolicitud, numeroIdentificacion, codigoRespuesta, mensajeRespuesta));        
    }
    @When("^I inquiry the AS400 in file ATT0119 whith numeroSolicitud: \"([^\"]*)\" and tipoIdentificacion: \"([^\"]*)\" and numeroIdentificacion: \"([^\"]*)\"$")
    public void iInquiryTheASInFileATTWhithNumeroSolicitudAndTipoIdentificacionAndNumeroIdentificacion(String numeroSolicitud, String tipoIdentificacion, String numeroIdentificacion) {
    	theActorInTheSpotlight().attemptsTo(CreateConnection.toAs400(URL_DATABASE).withUserName(USER_AS400).andPassword(PASSWORD_AS400),
                Consult.logWithQuery(QUERY_ATT0119).withParameters(numeroSolicitud, numeroIdentificacion));
    }    
    

    @Then("^I should see that the response ATT0119 has numeroSolicitud: \"([^\"]*)\" and tipoIdentificacion: \"([^\"]*)\" and numeroIdentificacion: \"([^\"]*)\" and mensajeRespuestaAs400: \"([^\"]*)\"$")
    public void iShouldSeeThatTheResponseATT0119HasNumeroSolicitudAndTipoIdentificacionAndNumeroIdentificacionAndMensajeRespuestaAs(String numeroSolicitud,String tipoIdentificacion, String numeroIdentificacion,String mensajeRespuestaAs400) {
    	if(!mensajeRespuestaAs400.equals("vacio")) {
    		theActorInTheSpotlight().attemptsTo(ValidateTheLastResponseOfAs400.ofFields("NUMSOL","NUMIDF").hasTheItems(String.valueOf(Integer.parseInt(numeroSolicitud)), numeroIdentificacion));	
    	}    	       
    }
}
