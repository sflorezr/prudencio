package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class ObtenerPdfPagareService {
    public static final String ROOT_PATH_XML_OBTENER_PDF_PAGARE="Envelope.Body.obtenerPdfPagareResponse";
    public static final String FIELD_CODIGO_RESPUESTA_RESPONSE="codigoRespuesta";
    public static final String FIELD_MENSAJE_RESPUESTA_RESPONSE="mensajeRespuesta";
    public static final String FIELD_ESTADO_PAGARE_RESPONSE="estadoPagare";
    public static final String FIELD_FECHA_RESPONSE="fechaGrabacionPagare";
    public static final String FIELD_ID_PAGARE_RESPONSE="idPagareDeceval";
    public static final String FIELD_NOMBRE_OTORGANTE_RESPONSE="nombreOtorgante";
    public static final String FIELD_DOCUMENTO_OTORGANTE_RESPONSE="numeroDocumentoOtorgante";
    public static final String FIELD_TIPO_DOCUMENTO_RESPONSE="tipoDocumentoOtorgante";

    private ObtenerPdfPagareService() {
    }
}
