#language:en
Feature: Obtener pagare
  As an User
  I want to Inquiry a prommisory note
  to see if the customer has already signed it

  @Ejecutar
  Scenario Outline: Consulta Servicio Obtener Pagare y ATT0119 en el AS400

    When I inquiry the numeroSolicitud:"<numSolicitud>" of a customer with tipoIdentificacion:"<tipoIdentificacion>" and numeroIdentificacion:"<numeroIdentificacion>"
    Then I should see that the response has codigoRespuesta:"<codigoRespuesta>" and mensajeRespuesta:"<mensajeRespuesta>" and estado:"<estado>" with fecha:"<fecha>" and hora:"<hora>"
    When I inquiry the AS400 in file ATT0117 whith numeroSolicitud: "<numSolicitud>" and tipoIdentificacion: "<tipoIdentificacion>" and numeroIdentificacion: "<numeroIdentificacion>" and codigoRespuesta: "<codigoRespuestaAs400>" and mensajeRespuesta: "<mensajeRespuestaAs400>"
    Then I should see that the response has numeroSolicitud: "<numSolicitud>" and tipoIdentificacion: "<tipoIdentificacion>" and numeroIdentificacion: "<numeroIdentificacion>" and codigoRespuesta: "<codigoRespuestaAs400>" and mensajeRespuesta: "<mensajeRespuestaAs400>"
    When I inquiry the AS400 in file ATT0119 whith numeroSolicitud: "<numSolicitud>" and tipoIdentificacion: "<tipoIdentificacion>" and numeroIdentificacion: "<numeroIdentificacion>"
    Then I should see that the response ATT0119 has numeroSolicitud: "<numSolicitud>" and tipoIdentificacion: "<tipoIdentificacion>" and numeroIdentificacion: "<numeroIdentificacion>" and mensajeRespuestaAs400: "<mensajeRespuestaAs400>"

    Examples:
      | numSolicitud | tipoIdentificacion | numeroIdentificacion | codigoRespuesta | mensajeRespuesta   | estado | fecha | hora | codigoRespuestaAs400 | mensajeRespuestaAs400 |
      | 1            | 1                  | 1                    | OK01            | PAGARE NO GENERADO | D0118  |       |      | vacio                | vacio                 |
      | 0            | 1                  | 0                    | OK01            | PAGARE NO GENERADO | D0118  |       |      | vacio                | vacio                 |
      