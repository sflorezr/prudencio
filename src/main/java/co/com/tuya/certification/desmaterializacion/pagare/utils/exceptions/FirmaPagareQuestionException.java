package co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions;

public class FirmaPagareQuestionException extends AssertionError {
	public FirmaPagareQuestionException() {
	}

	public FirmaPagareQuestionException(Object detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(boolean detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(char detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(int detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(long detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(float detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(double detailMessage) {
		super(detailMessage);
	}

	public FirmaPagareQuestionException(String message, Throwable cause) {
		super(message, cause);
	}
}
