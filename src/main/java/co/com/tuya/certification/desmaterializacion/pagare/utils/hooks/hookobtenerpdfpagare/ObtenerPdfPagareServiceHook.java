package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookobtenerpdfpagare;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.QueryTheDatabase;
import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.XML_OBTENER_PDF_PAGARE;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class ObtenerPdfPagareServiceHook {
    private static final String USER_HOMINI = "USER HOMINI";
    @Before
    public void setup(){
        setTheStage(new OnlineCast());
        theActorCalled(USER_HOMINI).whoCan(CallAnApi.at("https://brokerlab.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_OBTENER_PDF_PAGARE));
        theActorInTheSpotlight().whoCan(QueryTheDatabase.as400());

    }
    @After
    public void finishSetup(){
        drawTheCurtain();
    }
}
