package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class ValidarClienteRegistraduria {
    public static final String ROOT_PATH_XML_VALIDAR_CLIENTE_REGISTRADURIA="Envelope.Body.validarClienteRegistraduriaResponse";
    public static final String FIELD_CODIGO_RESPUESTA_RESPONSE="codigoRespuesta";
    public static final String FIELD_MENSAJE_RESPUESTA_RESPONSE="mensajeRespuesta";
    public static final String FIELD_ESTADO_VALIDACION_RESPONSE="estadoValidacion";
    public static final String FIELD_OBSERVACION_RESPONSE="observacion";
    public static final String FIELD_CAPTURA_HUELLA_RESPONSE="capturaHuella";    

    private ValidarClienteRegistraduria() {
    }
}
