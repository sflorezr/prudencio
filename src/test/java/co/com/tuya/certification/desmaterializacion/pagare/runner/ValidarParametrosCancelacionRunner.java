package co.com.tuya.certification.desmaterializacion.pagare.runner;

import co.com.tuya.certification.desmaterializacion.pagare.utils.TY_ChangeLogo;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import static cucumber.api.SnippetType.CAMELCASE;

@CucumberOptions(
        features = "src//test//resources//features//ValidaParametrosCancelacion.feature",
        glue = {"co.com.tuya.certification.desmaterializacion.pagare.stepdefinitions","co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookvalidarregistraduria"},
        snippets = CAMELCASE

)
@RunWith(CucumberWithSerenity.class)
public class ValidarParametrosCancelacionRunner {
	static TY_ChangeLogo logoSerenityReport;

	@AfterClass
	public static void finishTestExe() {
		logoSerenityReport = new TY_ChangeLogo();
		logoSerenityReport.changeLogo();
	}
}
