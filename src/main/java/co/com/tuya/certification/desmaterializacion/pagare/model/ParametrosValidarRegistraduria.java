package co.com.tuya.certification.desmaterializacion.pagare.model;

public class ParametrosValidarRegistraduria {
	private String a50Tip;
	private String a50Con;
	
	public ParametrosValidarRegistraduria(String a50Tip, String a50Con) {
		this.a50Tip = a50Tip;
		this.a50Con = a50Con;
	}
	public String getA50Tip() {
		return a50Tip;
	}
	public String getA50Con() {
		return a50Con;
	}
	

}
