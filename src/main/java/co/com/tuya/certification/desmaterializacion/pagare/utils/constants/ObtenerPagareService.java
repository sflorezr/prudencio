package co.com.tuya.certification.desmaterializacion.pagare.utils.constants;

public class ObtenerPagareService {
    public static final String ROOT_PATH_XML_OBTENER_PAGARE="Envelope.Body.obtenerPagareResponse";
    public static final String FIELD_CODIGO_RESPUESTA_RESPONSE="codigoRespuesta";
    public static final String FIELD_MENSAJE_RESPUESTA_RESPONSE="mensajeRespuesta";
    public static final String FIELD_ESTADO_RESPONSE="estado";
    public static final String FIELD_FECHA_RESPONSE="fecha";
    public static final String FIELD_HORA_RESPONSE="hora";

    private ObtenerPagareService() {
    }
}
