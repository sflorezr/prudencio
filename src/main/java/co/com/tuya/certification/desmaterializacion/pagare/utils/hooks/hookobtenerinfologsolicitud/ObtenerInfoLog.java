package co.com.tuya.certification.desmaterializacion.pagare.utils.hooks.hookobtenerinfologsolicitud;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.PathXml.XML_CONSULTAR_INFOLOG;
import static net.serenitybdd.screenplay.actors.OnStage.*;

public class ObtenerInfoLog {
    private static final String USER_SERVICE = "USER SERVICES";

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
        theActorCalled(USER_SERVICE).whoCan(CallAnApi.at("https://broker.tuya.corp:7083/intf/fw/ChannelAdapter/V2.0"));
        theActorInTheSpotlight().whoCan(ReadXml.fromPath(XML_CONSULTAR_INFOLOG));
    }
}
