package co.com.tuya.certification.desmaterializacion.pagare.utils.managers;

import java.util.ResourceBundle;

public class PropertiesManager {
	public static final ResourceBundle XmlPath = ResourceBundle.getBundle("XmlPath");
	public static final ResourceBundle QUERY_AS400 = ResourceBundle.getBundle("QueryAs400");
	public static final ResourceBundle ListaPDF = ResourceBundle.getBundle("pdfSolicitudPocisiones");
	public static final ResourceBundle ListaPDFCarulla = ResourceBundle.getBundle("pdfSolicitudPocisionesCarulla");

	private PropertiesManager() {
		throw new IllegalStateException("NO PUEDES INSTANCIAR UNA CLASE DE UTILIDADES");
	}
}
