package co.com.tuya.certification.desmaterializacion.pagare.abilities;

import java.util.Objects;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

import co.com.tuya.certification.desmaterializacion.pagare.utils.exceptions.ActorCannotQueryTheDatabase;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

public class CrearUnaConexionDeBaseDeDatos implements Ability{

	private final String usuario;
	private final String password;
	private final String urlBaseDeDatos;
	
	private CrearUnaConexionDeBaseDeDatos(String usuario, String password, String urlBaseDeDatos) {
		this.usuario = Objects.requireNonNull(usuario,"no puedes crear una conexion a base de datos sin usuario");
		this.password = Objects.requireNonNull(password,"no puedes crear una conexión a base de datos sin password");
		this.urlBaseDeDatos = Objects.requireNonNull(urlBaseDeDatos,"debes especificar a que ip o instancia para realizar la conexión con base de datos");
	}
	
	public static CrearUnaConexionDeBaseDeDatos conConfiguracion(String usuario, String password, String urlBaseDeDatos) {
		return new CrearUnaConexionDeBaseDeDatos(usuario, password, urlBaseDeDatos);
	}
	public static CrearUnaConexionDeBaseDeDatos como(Actor actor) {
		if(actor.abilityTo(CrearUnaConexionDeBaseDeDatos.class)==null) {
			throw new ActorCannotQueryTheDatabase(actor.getName());
		}
		return actor.abilityTo(CrearUnaConexionDeBaseDeDatos.class);
	}
	
	public Jdbi conectar() {
		return Jdbi.create(urlBaseDeDatos,usuario,password).installPlugin(new SqlObjectPlugin());
	}
		
}
