package co.com.tuya.certification.desmaterializacion.pagare.model.builder;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

import static co.com.tuya.certification.desmaterializacion.pagare.utils.constants.GenerarPagareService.*;

public class GenerarPagareServiceBuilder {
	private String numeroSolicitud;
	private String clave;
	private String tipoDocumento;
	private String numeroDocumento;
	private String contenido;

	public GenerarPagareServiceBuilder(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public static GenerarPagareServiceBuilder withNumeroSolicitud(String numeroSolicitud) {
		return new GenerarPagareServiceBuilder(numeroSolicitud);
	}

	public GenerarPagareServiceBuilder andClave(String clave) {
		this.clave = clave;
		return this;
	}

	public GenerarPagareServiceBuilder andTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
		return this;
	}

	public GenerarPagareServiceBuilder andNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
		return this;
	}

	public GenerarPagareServiceBuilder andContenido(String contenido) {
		this.contenido = contenido;
		return this;
	}

	public <T extends Actor> String buildForActor(T actor) {

		return ReadXml.as(actor).resolve().replace(NUMERO_SOLICITUD_XML_TAG, numeroSolicitud)
				.replace(CLAVE_XML_TAG, clave).replace(TIPO_DOCUMENTO_XML_TAG, tipoDocumento)
				.replace(NUMERO_DOCUMENTO, numeroDocumento).replace(CONTENIDO, contenido);
	}
}
