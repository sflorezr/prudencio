package co.com.tuya.certification.desmaterializacion.pagare.model;

import co.com.tuya.certification.desmaterializacion.pagare.abilities.ReadXml;
import net.serenitybdd.screenplay.Actor;

public class ValidarClienteRegistraduria {
    private String tipoIdentificacion;
    private String identificacion;

    public ValidarClienteRegistraduria(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public static ValidarClienteRegistraduria withTipoIdentificacion(String tipoIdentificacion){
        return new ValidarClienteRegistraduria(tipoIdentificacion);
    }
    
    public ValidarClienteRegistraduria andIdentificacion(String identificacion) {
        this.identificacion = identificacion;
        return this;
    }
    public<T extends Actor> String buildRequestForActor(T actor){
        return ReadXml.as(actor).resolve().replace("valortipoIdentificacion",tipoIdentificacion).replace("valoridentificacion", identificacion);
    }


}
